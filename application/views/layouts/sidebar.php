<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<li><a href="<?=base_url()?>admin/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
			<li><a href="<?=base_url()?>admin/category/list"><i class="fa fa-tag"></i> <span>Category</span></a></li>
			<li><a href="<?=base_url()?>admin/subcategory/list"><i class="fa fa-list-alt"></i> <span>Sub Category</span></a></li>
			<li><a href="<?=base_url()?>admin/branch/list"><i class="fa fa-map-marker"></i> <span>Branches Network</span></a></li>
			<li><a href="<?=base_url()?>admin/client/list"><i class="fa fa-user-circle-o"></i> <span>Clients</span></a></li>
			<li><a href="<?=base_url()?>admin/testimonials/list"><i class="fa fa-file"></i> <span>Testimonials</span></a></li>
			<li><a href="<?=base_url()?>admin/download/list"><i class="fa fa-download"></i> <span>Download Files</span></a></li>
			<li><a href="<?=base_url()?>admin/user/list"><i class="fa fa-user"></i> <span>User Managements</span></a></li>
		</ul>
	</section>
</aside>