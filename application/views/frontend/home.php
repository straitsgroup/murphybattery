<?php $this->load->view('frontend/layouts/header');?>
<header class="masthead">
	<div class="container">
		<div class="intro-text">
			<div class="intro-heading">Automotive <br>Battery</div>
		</div>
	</div>
</header>

<!-- Services -->
<section class="">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Popular Categories</h2>
			</div>
			<!-- Top content -->
			<div class="top-content">
				<div class="container-fluid">
					<div id="carousel-example" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carousel-example" data-slide-to="0" class="active"></li>
							<li data-target="#carousel-example" data-slide-to="1"></li>
							<li data-target="#carousel-example" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner row w-100 mx-auto" role="listbox">
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active">
								<img src="<?= base_url()?>webassets/images/MP65.jpg" class="img-fluid mx-auto d-block hvr" alt="img1">
								<div class="carousel-title text-center">Murphy Battery</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<img src="<?= base_url()?>webassets/images/carousal2.png" class="img-fluid mx-auto d-block" alt="img2">
								<div class="carousel-title text-center">Sanso</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<img src="<?= base_url()?>webassets/images/carousal1.png" class="img-fluid mx-auto d-block" alt="img3">
								<div class="carousel-title text-center">Elak Charger And Tester</div>
							</div>
							
						</div>
						<a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="bg-light mt80">
	<div class="container pt-5 pb-5">
		<div class="row pt-3">
			<div class="col-md-7 col-sm-7 bg-white pb-3">
				<h2 class="section-heading">About Us</h2>
				<p>
					Murphy Battery is one of the leading firms in India well known for all types of Automotive, Industrial, Electric Vehicle and Lithium Batteries for varied applications, Battery Testing & Charging Equipment, Inverters, UPS and more.. <br>

					Started In the year 1968 has excellent infrastructure and team of trained technicians to provide a great customer experience. The Sales team guides our customers to choose the right battery/ups for their applications. The Service Team is geared up to provide pre and post sales service. Most important, We value our customers, their time and money.
				</p>
				<a href="<?= base_url()?>about-us" class="btn btn-primary">Read More <i class="fa fa-long-arrow-right"></i></a>
			</div>
			<div class="col-md-5 col-sm-5 pl-0 my-auto">
				<img src="<?= base_url()?>webassets/images/about-us.png" class="img-fluid">
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container pt-5 pb-5">
		<div class="col-lg-12 text-center">
			<h2 class="section-heading">Services</h2>
		</div>
		<div class="row pt-3">
			<div class="col-lg-6 my-auto">
				<img src="<?= base_url()?>webassets/images/services.png" class="img-fluid">
			</div>
			<div class="col-lg-6 bg-white pb-3">
				<ul class="list-unstyled">
					<a href="#" class="service-link" onmouseover="hover(this,'battery-charging-w.png','charging');" onmouseout="unhover(this,'battery-charging-r.png','charging');">
						<li class="service-area mb-3">
							<img src="<?= base_url()?>webassets/images/battery-charging-r.png" class="img-fluid pl-3" id="charging">
							<span class="pl-3">Battery Charging </span>
						</li>
					</a>
					<a href="#" class="service-link" onmouseover="hover(this,'battery-installation-w.png','installation');" onmouseout="unhover(this,'battery-installation-r.png','installation');">
						<li class="service-area mb-3">
							<img src="<?= base_url()?>webassets/images/battery-installation-r.png" class="img-fluid pl-3" id="installation"> 
							<span class="pl-3">Battery Installation </span>
						</li>
					</a>
					<a href="#" class="service-link" onmouseover="hover(this,'battery-testing-w.png','testing');" onmouseout="unhover(this,'battery-testing-r.png','testing');">
						<li class="service-area mb-3">
							<img src="<?= base_url()?>webassets/images/battery-testing-r.png" class="img-fluid pl-3" id="testing"> 
							<span class="pl-3">Battery Testing </span>
						</li>
					</a>
					<a href="#" class="service-link" onmouseover="hover(this,'battery-repairing-w.png','repairing');" onmouseout="unhover(this,'battery-repairing-r.png','repairing');">
						<li class="service-area mb-3">
							<img src="<?= base_url()?>webassets/images/battery-repairing-r.png" class="img-fluid pl-3" id="repairing"> 
							<span class="pl-3">Battery Repairing (for the Repairable Types) </span>
						</li>
					</a>
					<a href="#" class="service-link" onmouseover="hover(this,'ups-inverter-w.png','ups');" onmouseout="unhover(this,'ups-inverter-r.png','ups');">
						<li class="service-area mb-3">
							<img src="<?= base_url()?>webassets/images/ups-inverter-r.png" class="img-fluid pl-3" id="ups"> 
							<span class="pl-3">UPS/Inverter Installation and Repair </span>
						</li>
					</a>
					<a href="#" class="service-link" onmouseover="hover(this,'info-w.png','info');" onmouseout="unhover(this,'info-r.png','info');">
						<li class="service-area mb-3">
							<img src="<?= base_url()?>webassets/images/info-r.png" class="img-fluid pl-3" id="info"> 
							<span class="pl-3">Do's and Dont's </span>
						</li>
					</a>
					<a href="#" class="service-link" onmouseover="hover(this,'breakdown-service-assistance-w.png','break');" onmouseout="unhover(this,'breakdown-service-assistance-r.png','break');">
						<li class="service-area mb-3">
							<img src="<?= base_url()?>webassets/images/breakdown-service-assistance-r.png" class="img-fluid pl-3" id="break"> 
							<span class="pl-3">Breakdown Service Assistance </span>
						</li>
					</a>
					<a href="#" class="service-link" onmouseover="hover(this,'annual-maintenance-contracts-w.png','annual');" onmouseout="unhover(this,'annual-maintenance-contracts-r.png','annual');">
						<li class="service-area mb-3">
							<img src="<?= base_url()?>webassets/images/annual-maintenance-contracts-r.png" class="img-fluid pl-3" id="annual"> 
							<span class="pl-3">Annual Maintenance Contracts </span>
						</li>
					</a>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="page-section" id="why-murphy">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading mb-5">Why Murphy ?</h2>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-md-3 col-sm-6">
				<div class="why-tag">
					<img class="img-fluid d-block mx-auto pb-4" src="<?= base_url()?>webassets/images/technology.png" alt="Well Known Brand">
					<div>Well Known <br> Brands</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="why-tag">
					<img class="img-fluid d-block mx-auto pb-4" src="<?= base_url()?>webassets/images/iso-certified.png" alt="Quality Services">
					<div>Quality  <br> Services</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="why-tag">
					<img class="img-fluid d-block mx-auto pb-4" src="<?= base_url()?>webassets/images/trusted-brand.png" alt="50 + Years of Trust">
					<div>50 + Years  <br>of Trust</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="why-tag">
					<img class="img-fluid d-block mx-auto pb-4" src="<?= base_url()?>webassets/images/quality-battery.png" alt="Value Customers">
					<div>Value <br> Customers</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="py-5">
	<div class="container">
		<div class="col-lg-12 text-center">
			<h2 class="section-heading mb-0">Service Network</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		</div>
		<div class="row text-center mt-4">
			<div class="col-md-3 col-sm-6">
				<div class="sevice-box">
					<div class="num-bold">1235+</div>
					Dealler Network             
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="sevice-box">
					<div class="num-bold">592+</div>
					Sales & Service Team             
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="sevice-box">
					<div class="num-bold">627+</div>
					Distributor Network             
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="sevice-box">
					<div class="num-bold">10,569+</div>
					Happy Customers         
				</div>
			</div>
		</div>
	</div>
</section>

<?php $this->load->view('frontend/layouts/footer');?>