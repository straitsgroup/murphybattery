<?php $this->load->view('frontend/layouts/header');?>
<header class="automotive">
	<div class="container">
		<div class="row text-center">
			<div class="intro-text col-lg-12">
				<h1><?= $product['name'] ?> </h1>
			</div>		
		</div>
	</div>
</header>
<section class="bg-light">
	<div class="container py-2">
		<a href="<?= base_url()?>"><i class="fa fa-home clr-red f20 pr-3"></i></a> <i class="fa fa fa-angle-right f20"></i> <a href="<?= base_url()?>products" class="pl-3 pr-3"><span class="brd-cum"><?= $product['name'] ?></span> </a><i class="fa fa fa-angle-right f20"></i>  <span class="pl-3 brd-cum"><?= $product['name'] ?></span>
	</div>
</section>
<section class="pb-5 pt-3">
	<div class="container contact-box">
		<div class="row mt-5">
			<div class="col-md-12 mb-4">
				<h4 class="clr-red mb-3 pt-4 pl-3"><b><?= $product['name'] ?></b></h4>
				<div class="banner-txt px-3 mb-4"><?= $product['description'] ?></div>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('frontend/layouts/enquiry-form');?>

<?php $this->load->view('frontend/layouts/footer');?>