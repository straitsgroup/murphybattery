<?php header('Content-type: text/xml'); ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo base_url()?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."about-us"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."products"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."automotive"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."industrial"?></loc>
        <priority>1</priority>
        <changefreq>monthly</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."e-bike"?></loc>
        <priority>1</priority>
        <changefreq>monthly</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."ups"?></loc>
        <priority>1</priority>
        <changefreq>monthly</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."contact-us"?></loc>
        <priority>1</priority>
        <changefreq>monthly</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."download"?></loc>
        <priority>1</priority>
        <changefreq>monthly</changefreq>
    </url>
</urlset>