<?php $this->load->view('frontend/layouts/header');?>
<header class="automotive">
	<div class="container">
		<div class="row text-center">
			<div class="intro-text col-lg-12">
				<h1>Downloads </h1>
			</div>		
		</div>
	</div>
</header>
<section class="bg-light">
	<div class="container py-2">
		<a href="<?= base_url()?>"><i class="fa fa-home clr-red f20 pr-3"></i></a> <i class="fa fa fa-angle-right f20"></i>  <span class="pl-3 brd-cum">Downloads</span>
	</div>
</section>
<section class="">
	<div class="container py-4">
		<h2 class="clr-red">Download Product Information</h2>
	</div>
</section>
<section class="bg-light mb-5">
	<div class="container py-4">
		<div class="row">
			<table class="table text-center table-striped">
				<thead class="bg-red clr-wht">
					<tr>
						<th>Brands</th>
						<th>Products</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php if ($downloads): ?>
						<?php foreach ($downloads as $download): ?>
							<tr>
								<td><?= $download['brand'] ?></td>
								<td><?= $download['product'] ?></td>
								<td><a href="<?= base_url()?>uploads/download/<?= $download['files'] ?>" class="btn download-btn clr-wht" download> Download &nbsp; <i class="fa fa-download clr-wht"></i></a></td>
							</tr>
						<?php endforeach ?>
					<?php endif ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>
<?php $this->load->view('frontend/layouts/enquiry-form');?>
<?php $this->load->view('frontend/layouts/footer');?>