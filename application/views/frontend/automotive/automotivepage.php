<?php $this->load->view('frontend/layouts/header');?>
<header class="automotive">
	<div class="container">
		<div class="row text-center">
			<div class="intro-text col-lg-12">
				<h1><?= $automotive['name']  ?> </h1>
			</div>		
		</div>
	</div>
</header>
<section class="bg-light">
	<div class="container py-2">
		<a href="<?= base_url()?>"><i class="fa fa-home clr-red f20 pr-3"></i></a> <i class="fa fa fa-angle-right f20"></i> <span class="pl-3 brd-cum"><?= $automotive['name']  ?></span>
	</div>
</section>
<section class="pb-5 pt-3">
	<div class="container">
		<div class="row text-center mt-5">
			<?php if ($sub_autmtve): ?>
				<?php foreach ($sub_autmtve as $Record): ?>
					<div class="col-md-6 mb-4">
						<div class="sevice-box py-5">
							<img src="<?= base_url()?>uploads/subcategory/<?= $Record['image'] ?>" class="img-fluid mb-3" alt="<?= $Record['name'] ?>">
							<h4 class="clr-red mb-3"><b><?= $Record['name'] ?></b></h4>
							<div class="banner-txt px-3 mb-4"><?= substr(strip_tags($Record['description']),0,120) ?></div>
							<a href="<?= base_url()?>automotive/<?= $Record['slug'] ?>" class="btn btn-primary">Read More <i class="fa fa-long-arrow-right"></i></a>
						</div>
					</div>
				<?php endforeach ?>
			<?php endif ?>
		</div>
	</div>
</section>
<?php $this->load->view('frontend/layouts/enquiry-form');?>
<?php $this->load->view('frontend/layouts/footer');?>