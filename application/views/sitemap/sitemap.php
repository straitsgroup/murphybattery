<?php header('Content-type: text/xml'); ?>
<?php echo'<?xml version="1.0" encoding="UTF-8" ?>' ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<sitemap>
		<loc><?php echo base_url()."sitemap/main-pages.xml";?></loc>
	</sitemap>    
	<sitemap>
		<loc><?php echo base_url()."sitemap/products.xml";?></loc>
	</sitemap>
	<sitemap>
		<loc><?php echo base_url()."sitemap/automotive.xml";?></loc>
	</sitemap>
	<sitemap>
		<loc><?php echo base_url()."sitemap/industrial.xml";?></loc>
	</sitemap>
	<sitemap>
		<loc><?php echo base_url()."sitemap/ebike.xml";?></loc>
	</sitemap>
	<sitemap>
		<loc><?php echo base_url()."sitemap/ups.xml";?></loc>
	</sitemap>
</sitemapindex>