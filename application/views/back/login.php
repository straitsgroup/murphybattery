<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?= $this->config->item('app_name') ?> | <?php echo $pagetitle ?></title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?php echo base_url().'assets/' ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url().'assets/' ?>bower_components/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url().'assets/' ?>bower_components/Ionicons/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url().'assets/' ?>dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?php echo base_url().'assets/' ?>plugins/iCheck/square/blue.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page" >
	<div class="login-box">
		<div class="login-logo">
			<h2><b><?= $this->config->item('app_name') ?></b></h2>
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">Sign in to Start session</p>
			<?php if($this->session->flashdata('msg')): ?>
				<div class="alert alert-danger">
					<strong>Error!</strong> <?php echo $this->session->flashdata('msg') ?>
				</div>
			<?php endif ?>
			<form method="post">
				<div class="form-group has-feedback">
					<input type="email" name="email" id="email" class="form-control" placeholder="Email">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="password" id="password" class="form-control" placeholder="Password">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-8">
					</div>
					<div class="col-xs-4">
						<button type="submit" class="btn btn-info btn-block btn-flat">Login</button>
					</div>
				</div>
			</form>
		</div>
		<div class="text-center"> <br>
			<a href="<?php echo base_url()?>" class="btn btn-default "> Back to Frontend</a>
		</div>
		<!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->

	<!-- jQuery 3 -->
	<script src="<?php echo base_url().'assets/' ?>bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url().'assets/' ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script src="<?php echo base_url().'assets/' ?>plugins/iCheck/icheck.min.js"></script>
</body>
</html>
