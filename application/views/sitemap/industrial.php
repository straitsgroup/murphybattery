<?php header('Content-type: text/xml'); ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <!-- Sitemap -->
    <?php if ($items): ?>
        <?php foreach($items as $item) { ?>
            <url>
                <loc>
                    <?php echo base_url()."industrial"."/".$item['slug'];?> 
                </loc>
                <lastmod><?php echo substr($item['created_at'],0,10);?></lastmod>
                <priority>0.9</priority>
                <changefreq>monthly</changefreq>
            </url>
        <?php } ?>

    <?php endif ?>

</urlset>