<?php $this->load->view('frontend/layouts/header');?>
<header class="automotive">
	<div class="container">
		<div class="row text-center">
			<div class="intro-text col-lg-12">
				<h1><?= $sub_page['name'] ?> </h1>
			</div>		
		</div>
	</div>
</header>
<section class="bg-light">
	<div class="container py-2">
		<a href="<?= base_url()?>"><i class="fa fa-home clr-red f20 pr-3"></i></a> <i class="fa fa fa-angle-right f20"></i> <a href="<?= base_url()?>lithium" class="pl-3 pr-3"><span class="brd-cum"><?= $lithium['name'] ?></span> </a><i class="fa fa fa-angle-right f20"></i>  <span class="pl-3 brd-cum"><?= $sub_page['name'] ?></span>
	</div>
</section>
<section class="pb-5 pt-3">
	<div class="container contact-box">
		<div class="row mt-5">
			<div class="col-md-12 mb-4">
				<img src="<?= base_url()?>uploads/subcategory/<?= $sub_page['image'] ?>" class="img-fluid p-3 float-left" alt="<?= $sub_page['name'] ?>">
				<h4 class="clr-red mb-3 pt-4 pl-3"><b><?= $sub_page['name'] ?></b></h4>
				<div class="banner-txt px-3 mb-4"><?= $sub_page['description'] ?></div>
			</div>
		</div>
	</div>
</section>
<section class="bg-light mb-5">
	<div class="container py-4">
		<h3 class="clr-red mb-3"> More <?= $lithium['name'] ?> Products</h3>

		<div class="row">
			<?php if ($sub_autmtve): ?>
				<?php foreach ($sub_autmtve as $Record): ?>
					<?php if ($Record['name'] != $sub_page['name']): ?>
						<div class="col-md-4 mb-4 text-center">
							<div class="sevice-box py-3 bg-white">
								<img src="<?= base_url()?>uploads/subcategory/<?= $Record['image'] ?>" class="img-fluid mb-3" alt="<?= $Record['name'] ?>">
								<a href="<?= base_url()?>lithium/<?= $Record['slug'] ?>">
									<div class="carousel-title mt-3"><?= $Record['name'] ?></div>
								</a>
							</div>
						</div>
					<?php endif ?>
				<?php endforeach ?>
			<?php endif ?>
		</div>
	</div>
</div>
</section>
<?php $this->load->view('frontend/layouts/enquiry-form');?>
<?php $this->load->view('frontend/layouts/footer');?>