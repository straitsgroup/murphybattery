<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FrontendModel extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getAll($tbl,$ord="desc")
	{
		$this->db->where('status', 1);
		$this->db->order_by('id',$ord);
		$sql = $this->db->get($tbl);
		if ($sql->num_rows()>0)
		{
			return $sql->result_array();
		}
		else
		{
			return false;
		}
	}
	public function getBranch($tbl,$state,$city,$ord="desc")
	{
		$this->db->where('state', $state);
		$this->db->where('city', $city);
		$this->db->where('status', 1);
		$this->db->order_by('id',$ord);
		$sql = $this->db->get($tbl);
		if ($sql->num_rows()>0)
		{
			return $sql->result_array();
		}
		else
		{
			return false;
		}
	}


	public function getGroup($tbl,$field,$ord="desc")
	{
		$this->db->select($field);
		$this->db->distinct($field);
		$this->db->where('status', 1);
		$sql = $this->db->get($tbl);
		if ($sql->num_rows()>0)
		{
			return $sql->result_array();
		}
		else
		{
			return false;
		}
	}

	public function getFile($tbl,$brand,$product)
	{
		$this->db->where('brand', $brand);
		$this->db->where('product', $product);
		$this->db->where('status', 1);
		$sql = $this->db->get($tbl);
		if ($sql->num_rows()==1)
		{
			return $sql->result_array()[0];
		}
		else
		{
			return false;
		}
	}

	public function getProduct($tbl,$brand){
		$this->db->select('product');
		$this->db->distinct('product');
		$this->db->where('brand', $brand);
		$this->db->where('status', 1);
		$sql = $this->db->get($tbl);
		if ($sql->num_rows()>0)
		{
			return $sql->result_array();
		}
		else
		{
			return false;
		}
	}

	public function getById($table, $id)
	{
		$this->db->where('id', $id);
		$this->db->where('status', 1);
		$sql= $this->db->get($table);
		if ($sql->num_rows() == 1)
		{
			return $sql->result_array()[0];
		}
		else
		{
			return false;
		}
	}
	public function getByrefId($table, $field,$value)
	{
		$this->db->where($field, $value);
		$this->db->where('status', 1);
		$sql= $this->db->get($table);
		if ($sql->num_rows() > 0)
		{
			return $sql->result_array();
		}
		else
		{
			return false;
		}
	}
	public function getSubPageBySlug($table, $field,$value,$slug)
	{
		$this->db->where($field, $value);
		$this->db->where('slug', $slug);
		$this->db->where('status', 1);
		$sql= $this->db->get($table);
		if ($sql->num_rows() == 1)
		{
			return $sql->result_array()[0];
		}
		else
		{
			return false;
		}
	}

	public function getBySlug($table, $slug)
	{
		$this->db->where('slug', $slug);
		$this->db->where('status', 1);
		$sql= $this->db->get($table);
		if ($sql->num_rows() == 1)
		{
			return $sql->result_array()[0];
		}
		else
		{
			return false;
		}
	}

	public function record_count($tbl) {
		return $this->db->count_all($tbl);
	}

	public function get_pagi($limit,$start, $tbl) {
		$this->db->limit($limit, $start);
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get($tbl);
		if ($sql->num_rows() > 0) {
			return $sql->result_array();
		} else {
			return false;
		}
	}

}