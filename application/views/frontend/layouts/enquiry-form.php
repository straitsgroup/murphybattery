<section class="mb-5">
	<div class="container">
		<div id="error">
			<?php if($this->session->flashdata('error')): ?>
				<div class="alert alert-danger">
					<strong>Info!</strong> <?php echo $this->session->flashdata('error') ?>
				</div>
			<?php endif ?>
		</div>
		<div id="msgdata">
			<?php if($this->session->flashdata('email_sent')): ?>
				<div class="alert alert-info">
					<strong>Info!</strong> <?php echo $this->session->flashdata('email_sent') ?>
				</div>
			<?php endif ?>
			<?php if($this->session->flashdata('email_sent_err')): ?>
				<div class="alert alert-danger">
					<strong>Info!</strong> <?php echo $this->session->flashdata('email_sent_err') ?>
				</div>
			<?php endif ?>
		</div>
		<form method="post" action="<?= base_url()?>enquiry-form">
			<div class="row  enq-box py-3">
				<div class="col-md-6 pl-5">
					<h2 class="heading-txt gr-clr">ENQUIRY</h2>
					<div class="form-group pr-5">
						<label class="lbl-txt" for="name">Enter Your Name <span class="clr-red">*</span></label>
						<input type="text" name="name" class="form-control" placeholder="Enter Your Name" id="name" value = "<?php echo set_value('name'); ?>" required >  
						<?php  if(form_error('name')){echo "<span style='color:red'>".form_error('name')."</span>";} ?>
					</div>
					<div class="form-group pr-5">
						<label class="lbl-txt" for="email">Enter Your Email <span class="clr-red">*</span></label>
						<input type="email" name="email" class="form-control" placeholder="Enter Your Email" id="email" required="" value = "<?php echo set_value('email'); ?>">
						<?php  if(form_error('email')){echo "<span style='color:red'>".form_error('email')."</span>";} ?>
					</div>
					<div class="form-group pr-5">
						<label class="lbl-txt" for="phone">Enter Your Phone Number <span class="clr-red">*</span></label>
						<input type="tel" name="phone" class="form-control" placeholder="Enter Your Phone Number" id="phone" value = "<?php echo set_value('phone'); ?>" required="">
						<?php  if(form_error('phone')){echo "<span style='color:red'>".form_error('phone')."</span>";} ?>
					</div>
					<div class="form-group pr-5">
						<label class="lbl-txt" for="message">Your Message <span class="clr-red">*</span></label>
						<textarea class="form-control" name="msg" rows="3" id="message" required=""><?php echo set_value('msg'); ?></textarea>
						<?php  if(form_error('msg')){echo "<span style='color:red'>".form_error('msg')."</span>";} ?>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
				<div class="col-md-6 brd-lft pl-5">
					<br><br>
					<h4 class="clr-red  mt-5"><b>Murphy Battery</b></h4>
					<address class="mt-4">
						32, Kumar Place, <br>
						2408, East Street, <br>
						Pune 411001 <br>
					</address>
					<span>
						<i class="fa fa-phone clr-red pr-2"></i>
						+91-20-30528800, <br>
						<span class="pl-4">+91-20-26348212</span>
					</span><br><br>
					<span>
						<i class="fa fa-envelope clr-red pr-2"></i>
						pw@murphybattery.com
					</span>
				</div>
			</div>
		</form>
	</div>
</section>