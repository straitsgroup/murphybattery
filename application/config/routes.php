<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Frontend';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*Frontend Routes*/
$route['about-us'] = 'Frontend/aboutUs';
$route['products'] = 'Frontend/Products';
$route['products/(:any)'] = 'Frontend/productSubpages/$1';

$route['automotive'] = 'Frontend/Automotive';
$route['automotive/(:any)'] = 'Frontend/automotiveSubpages/$1';

$route['lithium'] = 'Frontend/Lithium';
$route['lithium/(:any)'] = 'Frontend/lithiumSubpages/$1';

$route['industrial'] = 'Frontend/Industrial';
$route['industrial/(:any)'] = 'Frontend/industrialSubpages/$1';

$route['e-bike'] = 'Frontend/eBike';
$route['e-bike/(:any)'] = 'Frontend/eBikeSubpages/$1';

$route['ups'] = 'Frontend/Ups';
$route['ups/(:any)'] = 'Frontend/upsSubpages/$1';

$route['contact-us'] = 'Frontend/contactUs';
$route['enquiry-form'] = 'Frontend/EnqForm';
$route['filter'] = 'Frontend/filter';
$route['file-download'] = 'Frontend/fileDownload';
$route['downloads'] = 'Frontend/downloadFiles';
$route['productdata'] = 'Frontend/productData';
$route['newsletter'] = 'Frontend/newsLetter';

/*Sitemap*/
$route['sitemap\.xml'] = "Sitemap/sitemap";
$route['sitemap'] = "Sitemap/sitemap";

$route['sitemap/main-pages\.xml']="Sitemap/StaticUrls";
$route['sitemap/main-pages']="Sitemap/StaticUrls";

$route['sitemap/products\.xml']="Sitemap/products";
$route['sitemap/products']="Sitemap/products";

$route['sitemap/automotive\.xml']="Sitemap/automotive";
$route['sitemap/automotive']="Sitemap/automotive";

$route['sitemap/industrial\.xml']="Sitemap/industrial";
$route['sitemap/industrial']="Sitemap/industrial";

$route['sitemap/ebike\.xml']="Sitemap/ebike";
$route['sitemap/ebike']="Sitemap/ebike";

$route['sitemap/ups\.xml']="Sitemap/ups";
$route['sitemap/ups']="Sitemap/ups";




/*Backend Routes*/
$route['login'] = 'admin/login';
$route['logout'] = 'admin/logout';
$route['admin/dashboard'] = 'backend/dashboard';

/*User */
$route['admin/user/list']='UserController';
$route['admin/user/add']='UserController/add';
$route['admin/user/edit/(:any)']='UserController/edit/$1';
$route['admin/user/enable/(:any)']='UserController/enable/$1';
$route['admin/user/disable/(:any)']='UserController/disable/$1';
$route['admin/user/delete/(:any)']='UserController/delete/$1';
$route['admin/change-password']='UserController/changePassword';

/*Pages */
$route['admin/pages/list']='PagesController';
$route['admin/pages/add']='PagesController/add';
$route['admin/pages/edit/(:any)']='PagesController/edit/$1';
$route['admin/pages/enable/(:any)']='PagesController/enable/$1';
$route['admin/pages/disable/(:any)']='PagesController/disable/$1';
$route['admin/pages/delete/(:any)']='PagesController/delete/$1';

/*Settings */
$route['admin/settings/list']='SettingsController';
$route['admin/settings/add']='SettingsController/add';
$route['admin/settings/edit/(:any)']='SettingsController/edit/$1';
$route['admin/settings/enable/(:any)']='SettingsController/enable/$1';
$route['admin/settings/disable/(:any)']='SettingsController/disable/$1';
$route['admin/settings/delete/(:any)']='SettingsController/delete/$1';


/*Blogs*/
$route['admin/blog/list']='BlogController';
$route['admin/blog/add']='BlogController/add';
$route['admin/blog/edit/(:any)']='BlogController/edit/$1';
$route['admin/blog/enable/(:any)']='BlogController/enable/$1';
$route['admin/blog/disable/(:any)']='BlogController/disable/$1';
$route['admin/blog/delete/(:any)']='BlogController/delete/$1';

/*Products*/
$route['admin/product/list']='ProductController';
$route['admin/product/add']='ProductController/add';
$route['admin/product/edit/(:any)']='ProductController/edit/$1';
$route['admin/product/enable/(:any)']='ProductController/enable/$1';
$route['admin/product/disable/(:any)']='ProductController/disable/$1';
$route['admin/product/delete/(:any)']='ProductController/delete/$1';

/*Client*/
$route['admin/client/list']='ClientController';
$route['admin/client/add']='ClientController/add';
$route['admin/client/edit/(:any)']='ClientController/edit/$1';
$route['admin/client/enable/(:any)']='ClientController/enable/$1';
$route['admin/client/disable/(:any)']='ClientController/disable/$1';
$route['admin/client/delete/(:any)']='ClientController/delete/$1';

/*Category*/
$route['admin/category/list']='CategoryController';
$route['admin/category/add']='CategoryController/add';
$route['admin/category/edit/(:any)']='CategoryController/edit/$1';
$route['admin/category/enable/(:any)']='CategoryController/enable/$1';
$route['admin/category/disable/(:any)']='CategoryController/disable/$1';
$route['admin/category/delete/(:any)']='CategoryController/delete/$1';

/*Sub Category*/
$route['admin/subcategory/list']='SubcategoryController';
$route['admin/subcategory/add']='SubcategoryController/add';
$route['admin/subcategory/edit/(:any)']='SubcategoryController/edit/$1';
$route['admin/subcategory/enable/(:any)']='SubcategoryController/enable/$1';
$route['admin/subcategory/disable/(:any)']='SubcategoryController/disable/$1';
$route['admin/subcategory/delete/(:any)']='SubcategoryController/delete/$1';

/*Testimonials*/
$route['admin/testimonials/list']='TestimonialsController';
$route['admin/testimonials/add']='TestimonialsController/add';
$route['admin/testimonials/edit/(:any)']='TestimonialsController/edit/$1';
$route['admin/testimonials/enable/(:any)']='TestimonialsController/enable/$1';
$route['admin/testimonials/disable/(:any)']='TestimonialsController/disable/$1';
$route['admin/testimonials/delete/(:any)']='TestimonialsController/delete/$1';

/*Downloads*/
$route['admin/download/list']='DownloadController';
$route['admin/download/add']='DownloadController/add';
$route['admin/download/edit/(:any)']='DownloadController/edit/$1';
$route['admin/download/enable/(:any)']='DownloadController/enable/$1';
$route['admin/download/disable/(:any)']='DownloadController/disable/$1';
$route['admin/download/delete/(:any)']='DownloadController/delete/$1';

/*Branches*/
$route['admin/branch/list']='BranchController';
$route['admin/branch/add']='BranchController/add';
$route['admin/branch/edit/(:any)']='BranchController/edit/$1';
$route['admin/branch/enable/(:any)']='BranchController/enable/$1';
$route['admin/branch/disable/(:any)']='BranchController/disable/$1';
$route['admin/branch/delete/(:any)']='BranchController/delete/$1';