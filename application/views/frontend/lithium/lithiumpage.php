<?php $this->load->view('frontend/layouts/header');?>
<header class="automotive">
	<div class="container">
		<div class="row text-center">
			<div class="intro-text col-lg-12">
				<h1><?= $lithium['name']  ?> </h1>
			</div>		
		</div>
	</div>
</header>
<section class="bg-light">
	<div class="container py-2">
		<a href="<?= base_url()?>"><i class="fa fa-home clr-red f20 pr-3"></i></a> <i class="fa fa fa-angle-right f20"></i><span class="pl-3 brd-cum"><?= $lithium['name']  ?></span>
	</div>
</section>
<section class="my-5">
	<div class="container">
		<?php if ($sub_autmtve): ?>
			<?php $count=1; ?>
			<?php foreach ($sub_autmtve as $Record): ?>
				<?php if ($count%2==0): ?>
					<div class="row indus-box mb-5">			
						<div class="col-lg-5 text-center indus-bg2 py-3">
							<img src="<?= base_url()?>uploads/subcategory/<?= $Record['image'] ?>" class="img-fluid" alt="<?= $Record['name'] ?>">
						</div>
						<div class="col-lg-7 py-5 pr-5">
							<h4 class="clr-red mb-3"><b><?= $Record['name'] ?></b></h4>
							<div class="banner-txt mb-4 text-justify"><?= substr(strip_tags($Record['description']),0,800) ?></div>
							<a href="<?= base_url()?>lithium/<?= $Record['slug'] ?>" class="btn btn-primary">Read More <i class="fa fa-long-arrow-right"></i></a>
						</div>
					</div>
					<?php else: ?>
						<div class="row indus-box mb-5">
							<div class="col-lg-7 py-5 pl-5">
								<h4 class="clr-red mb-3"><b><?= $Record['name'] ?></b></h4>
								<div class="banner-txt mb-4 text-justify"><?= substr(strip_tags($Record['description']),0,800) ?></div>
								<a href="<?= base_url()?>lithium/<?= $Record['slug'] ?>" class="btn btn-primary">Read More <i class="fa fa-long-arrow-right"></i></a>
							</div>
							<div class="col-lg-5 text-center indus-bg py-3">
								<img src="<?= base_url()?>uploads/subcategory/<?= $Record['image'] ?>" class="img-fluid" alt="<?= $Record['name'] ?>">
							</div>
						</div>
					<?php endif ?>
					<?php $count+=1; ?>
				<?php endforeach ?>
			<?php endif ?>
		</div>
	</section>
	<?php $this->load->view('frontend/layouts/enquiry-form');?>
	<?php $this->load->view('frontend/layouts/footer');?>