<?php $this->load->view('frontend/layouts/header');?>
<header class="aboutus">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 text-left">
				<div class="intro-text">
					<div class="intro-heading"> is simply dummy text of  <br>dummy text </div>
					<p class="banner-txt">Murphy Battery is one of the leading firms in India well known for all types of Automotive ,Stationary Tubular , E-Bike Batteries and More....</p>
					<a href="<?= base_url()?>contact-us" class="btn btn-primary">Get In Touch <i class="fa fa-long-arrow-right"></i></a>
				</div>		
			</div>
			<div class="col-lg-6">
				<div class="intro-text">
					<img src="<?= base_url()?>webassets/images/services.png" class="img-fluid">
				</div>
			</div>
		</div>
		
	</div>
</header>

<section class="bg-light">
	<div class="container pt-5 pb-5">
		<div class="row pt-3">
			<div class="col-lg-5 pr-0">
				<img src="<?= base_url()?>webassets/images/aboutus/overview.png" class="img-fluid">
			</div>
			<div class="col-lg-7 my-3 bg-white">
				<h2 class="heading-txt">Overview</h2>
				<p>
					Murphy Battery is one of the leading firms in India well known for all types of Automotive, Stationary Tubular and E-Bike Batteries, Battery Parts, Battery Testing & Charging Equipment, Inverters, UPS and more..
					<br>
					In the year 1968 <span class="founder"> Mr. Subhashchandra Salhotra </span>started this venture. Today, his next generation continues the legacy complete with experience, backed by excellent infrastructure and team of trained technicians.
					<br>
					Murphy Battery has a network of dealers in Maharashtra and its own branches in Pune, Navi Mumbai, Kolhapur and Phaltan (Dist: Satara) with a complete team and testing facilities.
					<br>
					Murphy Battery has a range of Batteries for all applications, UPS systems and Inverters of all well known brands like <b>AMARON, EXIDE, MURPHY, HELLA, SANSO, SF SONIC </b>and more.
					<br>
					The range of Inverters & UPS for all applications are catered to from a bouquet of well known brands like <b>GENUS, LUMINOUS, MAHINDRA, MICROTEK, SUKAM </b> and more. .
					<br>
					The Battery Testing & Charging Equipments are the best from brands like <b>CINE ARC, ELAK, THIMSON </b> and more.
					<br>
					Our trained team guides our customers to choose the right battery/ups for their applications from the available range of options for warranty, price and capacity whichever best for the customer. We value our customers, their time and money. Our offers provide extremely competitive prices with no compromise on pre/post sales service making their life comfortable.
				</p>
			</div>
		</div>
	</div>
</section>

<section class="page-section" id="vision">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class=" clr-wht mb-4">V<span class="txt-undline">isio</span>n</h2>
			</div>
		</div>
		<div class="row text-center">
			<p class="section-heading">
				"Murphy Battery shall be recognized leader providing innovative products with great service in the field of batteries and power back up solutions of exceptional quality that provides value for our customers and a great place to work for our employees."
			</p>
		</div>
	</div>
</section>


<section class="" id="mission">
	<div class="container" id="values">
		<div class="row">
			<div class="col-lg-6 pl-5 py-5 bg-white">
				<h2 class="heading-txt">Mission</h2>
				<p>
					World class quality products and service <br>
					Innovative products that provides solutions to the customer’s problems<br>
					A morally responsible company guided by a strong Management<br>
					Contribute to the society by focus on greener products
				</p>
			</div>
			<div class="col-lg-6 pl-5 py-5 bg-light">
				<h2 class="heading-txt">Values</h2>
				<p>
					Respect for our customers and for each other in Murphy Battery. <br>
					Commitment to the company’s objectives.<br>
					Do our bit to make our planet a great place to live in.<br>
					Hold high regards for all – faiths, family, community & company.
				</p>
			</div>
		</div>
	</div>
</section>
<?php if ($testimonials): ?>
	<section class="py-5" id="testimonial">
		<div class="container ">
			<div class="row text-center testim-box py-5">
				<div class="col-lg-12 mb-4">
					<h2 class="mb-4">Tes<span class="txt-undline">timoni</span>als</h2>
					<h4>What people say about <span class="clr-red">Murphy Battery</span></h4>
				</div>
				<?php foreach ($testimonials as $testimonial): ?>
					<div class="col-lg-4 pb-4">
						<div class="testim-box">
							<div class="testim-box bg-red pt-4 pb-5 px-4">
								<div class="testim-txt mb-5">
									" <?= $testimonial['description'] ?>"
								</div>
							</div>
							<div class="bg-white pb-4">
								<img src="<?= base_url()?>uploads/testimonials/<?= $testimonial['image'] ?>" class="img-thumbnail mt-50"/> <br> <br>

								<span class="clr-red "><b><?= $testimonial['name'] ?> </b></span><br>
								<small class="gr-clr">
									<?= $testimonial['title'] ?>
									<?php if (!empty($testimonial['company'])): ?>
										,<?= $testimonial['company'] ?>
									<?php endif ?>
								</small>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</section>
<?php endif ?>
<?php $this->load->view('frontend/layouts/footer');?>