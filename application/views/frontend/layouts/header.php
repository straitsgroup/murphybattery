<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" type="image/png" href="<?= base_url()?>webassets/images/favicon.png">
  <title><?= $seo_title  ?> | Murphy Battery</title>

  <meta name="title" content="<?= $seo_title  ?>">
  <meta name="description" content="<?= $seo_description  ?>">
  <meta name="keywords" content=" <?= $seo_keywords  ?>">
  <link rel="canonical" href="<?= $cannonical_link  ?>">
  <meta name="author" content="Murphy Battery">
  <link href="<?= base_url()?>webassets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url()?>webassets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="<?= base_url()?>webassets/css/custom.css" rel="stylesheet">

</head>

<body id="page-top">
  <input type="hidden" id="base" value="<?= base_url(); ?>">
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="<?= base_url()?>">
        <img src="<?= base_url()?>webassets/images/logo.png" class="img-fluid logo">
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item ">
            <a class="nav-link js-scroll-trigger" href="<?= base_url()?>">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?= base_url()?>about-us">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?= base_url()?>products">Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?= base_url()?>automotive">Automotive</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?= base_url()?>industrial">Industrial</a>
          </li>
           <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?= base_url()?>lithium">Lithium</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?= base_url()?>e-bike">E-Bike</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?= base_url()?>ups">UPS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?= base_url()?>downloads">Downloads</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?= base_url()?>contact-us">Contact Us</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>