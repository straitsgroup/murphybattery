<?php $this->load->view('frontend/layouts/header');?>
<header class="automotive">
	<div class="container">
		<div class="row text-center">
			<div class="intro-text col-lg-12">
				<h1>Contact Us</h1>
			</div>		
		</div>
	</div>
</header>
<section class="bg-light">
	<div class="container py-2">
		<a href="<?= base_url()?>"><i class="fa fa-home clr-red f20 pr-3"></i></a> <i class="fa fa fa-angle-right f20"></i> <span class="pl-3 brd-cum">Contact Us</span>
	</div>
</section>
<section class="my-5">
	<div class="container">
		<form method="post" action="<?= base_url()?>contact-us">
			<?php if($this->session->flashdata('email_sent')): ?>
				<div class="alert alert-info">
					<strong>Info!</strong> <?php echo $this->session->flashdata('email_sent') ?>
				</div>
			<?php endif ?>
			<?php if($this->session->flashdata('email_sent_err')): ?>
				<div class="alert alert-danger">
					<strong>Info!</strong> <?php echo $this->session->flashdata('email_sent_err') ?>
				</div>
			<?php endif ?>

			<div class="row py-3">
				<div class="col-lg-5 bg-red my-5 clr-wht">
					<div class="py-5 pl-5 ">
						<br>
						<h4 class="clr-wht mt-5 pl-5"><b>Contact Us</b></h4>
						<address class="mt-4 pl-5 ">
							32, Kumar Place, <br>
							2408, East Street, <br>
							Pune 411001 <br>
						</address>
						<span class=" pl-5">
							<i class="fa fa-phone pr-2"></i>
							+91-20-30528800, <br>
						</span>
						<span class="pl-5 ">
							<span class="pr-2">&nbsp; &nbsp;</span>
						+91-20-26348212</span>
					</span><br><br>
					<span class=" pl-5">
						<i class="fa fa-envelope pr-2"></i>
						pw@murphybattery.com
					</span>
				</div>
			</div>
			<div class="col-lg-7 mt-3 p-5 contact-box">
				<h2 class="clr-red">Get In Touch</h2>
				<div class="form-group pr-5">
					<label class="lbl-txt" for="name">Enter Your Name <span class="clr-red">*</span></label>
					<input type="text" name="name" class="form-control" placeholder="Enter Your Name" id="name" value = "<?php echo set_value('name'); ?>" required >  
					<?php  if(form_error('name')){echo "<span style='color:red'>".form_error('name')."</span>";} ?>
				</div>
				<div class="form-group pr-5">
					<label class="lbl-txt" for="email">Enter Your Email <span class="clr-red">*</span></label>
					<input type="email" name="email" class="form-control" placeholder="Enter Your Email" id="email" required="" value = "<?php echo set_value('email'); ?>">
					<?php  if(form_error('email')){echo "<span style='color:red'>".form_error('email')."</span>";} ?>
				</div>
				<div class="form-group pr-5">
					<label class="lbl-txt" for="phone">Enter Your Phone Number <span class="clr-red">*</span></label>
					<input type="tel" name="phone" class="form-control" placeholder="Enter Your Phone Number" id="phone" value = "<?php echo set_value('phone'); ?>" required="">
					<?php  if(form_error('phone')){echo "<span style='color:red'>".form_error('phone')."</span>";} ?>
				</div>
				<div class="form-group pr-5">
					<label class="lbl-txt" for="message">Your Message <span class="clr-red">*</span></label>
					<textarea class="form-control" name="msg" rows="3" id="message" required=""><?php echo set_value('msg'); ?></textarea>
					<?php  if(form_error('msg')){echo "<span style='color:red'>".form_error('msg')."</span>";} ?>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>
	</div>
</section>
<section class="mt-5">
	<div class="container">
		<h2 class="clr-red">Network & Branches</h2>
		<div class="row">
			<div class="col-lg-5">
				<form method="post" id="filterform" >
					<div class="row">
						<div class="col-lg-8 p-5">
							<div class="form-group">
								<select name="state" class="form-control filter-area" id="state" required>
									<option disabled="" selected="" value="">State</option>
									<?php if ($states): ?>
										<?php foreach ($states as $state): ?>
											<option value="<?= $state ?>"><?= $state ?></option>
										<?php endforeach ?>
									<?php endif ?>
								</select>
								<i class="fa fa-chevron-down"></i>
							</div>
							<div class="form-group">
								<select name="city" class="form-control filter-area" id="city" required="">
									<option disabled="" selected="" value="">City</option>
									<?php if ($cities): ?>
										<?php foreach ($cities as $city): ?>
											<option value="<?= $city ?>"><?= $city ?></option>
										<?php endforeach ?>
									<?php endif ?>
								</select>
								<i class="fa fa-chevron-down"></i>
							</div>
							<!-- <div class="form-group">
								<select class="form-control filter-area" id="sel2">
									<option disabled="" selected="">Area</option>
									<option class="option-txt">1</option>
									<option class="option-txt">2</option>
									<option class="option-txt">3</option>
									<option class="option-txt">4</option>
								</select>
								<i class="fa fa-chevron-down"></i>
							</div> -->
						</div>
						<div class="col-lg-4 my-auto">
							<button type="submit" class="btn filter" id="filter"> <i class="fa fa-angle-right f40 clr-red"></i></button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-lg-7 pt-5" id="filter-data">
				<div class="contact-box py-3 pl-5 mb-3">
					<h4 class="clr-red">Pune</h4>
					<div class="mt-3">
						<span><i class="fas fa-map-marker-alt f20 clr-red"></i></span>
						<span class="pl-3 gr-clr">32, Kumar Place, 2408, East Street,Pune 411001</span>
					</div>
					<div class="mt-3">
						<span><i class="fas fa-phone-volume	 f20 clr-red"></i></span>
						<span class="pl-3 gr-clr">+91-20-30528800, +91-20-26348212</span>
					</div>
					<div class="mt-3">
						<span><i class="fa fa-envelope f20 clr-red"></i></span>
						<span class="pl-3 gr-clr">pw@murphybattery.com</span>
					</div>
				</div>
			</div>
		</section>
		<section class="my-5">
			<div class="container">

				<div class="map-responsive">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3783.453073179205!2d73.879383!3d18.508417!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c04853026b91%3A0xd236fe7f0ffd2358!2sMURPHY%20BATTERY!5e0!3m2!1sen!2sin!4v1587644278533!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen></iframe>
				</div>
			</div>
		</section>
		<?php $this->load->view('frontend/layouts/footer');?>
		<script type="text/javascript">
			$('#filterform').submit(function(e) {
				e.preventDefault();


				var state = $('#state').find(":selected").text();
				var city = $('#city').find(":selected").text();


				$.ajax({
					url: '<?=base_url()?>filter',
					type: 'POST',
					data: {state: state, city: city},
					error: function() {
						alert('Something is wrong');
					},
					success: function(data) {
						data1 =JSON.parse(data);
						var html="";
						data1.forEach(myFunction);
						function myFunction(item, index) {
							html += '<div class="contact-box py-3 pl-5 mb-3">\n\
							<h4 class="clr-red">'+item['city']+'</h4>\n\
							<div class="mt-3">\n\
							<span><i class="fas fa-map-marker-alt f20 clr-red"></i></span>\n\
							<span class="pl-3 gr-clr">'+item['address']+'</span>\n\
							</div>\n\
							<div class="mt-3">\n\
							<span><i class="fas fa-phone-volume	 f20 clr-red"></i></span>\n\
							<span class="pl-3 gr-clr">'+item['phone']+'</span>\n\
							</div>\n\
							<div class="mt-3">\n\
							<span><i class="fa fa-envelope f20 clr-red"></i></span>\n\
							<span class="pl-3 gr-clr">'+item['email']+'</span>\n\
							</div>\n\
							</div>';
						}
						$("#filter-data").html(html);
					}
				});


			});


		</script>