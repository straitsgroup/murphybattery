<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('BackendModel');
	}
	
	public function dashboard()
	{
		if ($this->session->userdata('logged_in')) {
			$data['pagetitle'] = 'Dashboard';
			$this->load->view('back/dashboard', $data);
		} else {
			redirect(base_url().'login');
		}
	}
}
