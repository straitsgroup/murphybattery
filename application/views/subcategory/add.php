<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Sub Category Management
			<small>Add Sub Category</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/subcategory/list">Sub Category</a></li>
			<li class="active">Add Sub Category</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<?php if($this->session->flashdata('msg')): ?>
					<div class="alert alert-info">
						<strong>Info!</strong> <?php echo $this->session->flashdata('msg') ?>
					</div>
				<?php endif ?>
				<div class="row">
					<form method="post" enctype="multipart/form-data">
						<div class="col-md-12">
							<div class="form-group">
								<label>Sub Category Image</label>
								<input type='file' class="form-control"  name="image" onchange="readURL(this);" required>	
								<img id="blah" src="http://placehold.it/180" alt="your image" class="pre-img" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Category</label>
								<select name="category_id" class="form-control select2" data-placeholder="" style="width: 100%;">
									<?php if ($categories): ?>
										<?php foreach ($categories as $category): ?>
											<option value="<?= $category['id'] ?>"><?= $category['name'] ?></option>
										<?php endforeach ?>
										<?php else: ?>
											<option value="1">No Category Added</option>
										<?php endif ?>
									</select>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Sub Category Name *</label>
									<input type="text" name="name" class="form-control" placeholder="Enter Name" required>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label>Description *</label>
									<textarea id="editor1" name='description' placeholder="Enter Description" required></textarea>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Meta Title *</label>
									<input type="text" name="seo_title" class="form-control" placeholder="Enter  Meta Title" required>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label> Meta Description *</label>
									<input type="text" name="seo_description" class="form-control" placeholder="Enter  Meta Description" required>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label> Meta Keyword *</label>
									<input type="text" name="seo_keywords" class="form-control" placeholder="Enter  Meta Keywords" required>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label> URL *</label>
									<input type="text" name="slug" class="form-control" placeholder="Enter  URL" required>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label> Canonical/URL *</label>
									<input type="text" name="cannonical_link" class="form-control" placeholder="Enter Canonical URL" required>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Status</label>
									<select name="status" class="form-control select2" data-placeholder="" style="width: 100%;">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
									</select>
								</div>
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('layouts/footer');?>