<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('FrontendModel');
	}
	public function sitemap(){
		$this->load->view('sitemap/sitemap');
	}
	public function StaticUrls(){
		$this->load->view('sitemap/staticpages');
	}
	
	public function products(){
		$data['items'] = $this->FrontendModel->getAll('category');
		$this->load->view('sitemap/products',$data);
	}
	public function automotive(){
		$automotive = $this->FrontendModel->getBySlug('category','automotive'); 
		$data['items'] = $this->FrontendModel->getByrefId('subcategory','category_id',$automotive['id']); 
		$this->load->view('sitemap/automotive',$data);
	}
	public function industrial(){
		$industrial = $this->FrontendModel->getBySlug('category','industrial'); 
		$data['items'] = $this->FrontendModel->getByrefId('subcategory','category_id',$industrial['id']); 
		$this->load->view('sitemap/industrial',$data);
	}
	public function ebike(){
		$ebike = $this->FrontendModel->getBySlug('category','e-bike'); 
		$data['items'] = $this->FrontendModel->getByrefId('subcategory','category_id',$ebike['id']); 
		$this->load->view('sitemap/ebike',$data);
	}
	public function ups(){
		$ups = $this->FrontendModel->getBySlug('category','ups'); 
		$data['items'] = $this->FrontendModel->getByrefId('subcategory','category_id',$ups['id']); 
		$this->load->view('sitemap/ups',$data);
	}
	
}

