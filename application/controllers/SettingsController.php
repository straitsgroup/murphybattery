<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SettingsController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('SettingsModel');
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->SettingsModel->getAll('settings'); 
			$data['pagetitle'] = 'Setting List';
			$this->load->view('settings/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$data = array(
					'app_name' =>$this->input->post('app_name') , 
					'email' =>$this->input->post('email') , 
					'phone' =>$this->input->post('phone') , 
					'address' =>$this->input->post('address') , 
					'url' =>$this->input->post('url') , 
					'header_files' =>$this->input->post('header_files') , 
					'facebook' =>$this->input->post('facebook') , 
					'linkdin' =>$this->input->post('linkdin') , 
					'twitter' =>$this->input->post('twitter') , 
					'google_plus' =>$this->input->post('google_plus') , 
					'instagram' =>$this->input->post('instagram') , 
					'status' =>$this->input->post('status') , 
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->SettingsModel->add($data, 'settings'))
				{
					$this->session->set_flashdata('msg', 'Record Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Record');
				}

				redirect(base_url().'admin/settings/add');

			}else{
				$data['pagetitle'] = 'Add Setting';
				$this->load->view('settings/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				

				$data = array(
					'app_name' =>$this->input->post('app_name') , 
					'email' =>$this->input->post('email') , 
					'phone' =>$this->input->post('phone') , 
					'address' =>$this->input->post('address') , 
					'url' =>$this->input->post('url') , 
					'header_files' =>$this->input->post('header_files') , 
					'facebook' =>$this->input->post('facebook') , 
					'linkdin' =>$this->input->post('linkdin') , 
					'twitter' =>$this->input->post('twitter') , 
					'google_plus' =>$this->input->post('google_plus') , 
					'instagram' =>$this->input->post('instagram') , 
					'status' =>$this->input->post('status') , 
					'updated_at'=>date("Y-m-d H:i:s"),
					'updated_by' => $this->session->userdata('username'), 

				);
				if ($this->SettingsModel->edit($data, 'settings', $id))
				{
					$this->session->set_flashdata('msg', 'Record Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Record');
				}
				redirect(base_url().'admin/settings/list');

			}else{
				$data['pagetitle'] = 'Edit Setting';
				$data['Record'] = $this->SettingsModel->getById('settings', $id);
				$this->load->view('settings/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->SettingsModel->delete('settings', $id))
			{
				$this->session->set_flashdata('msg', 'Record Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Record');
			}
			redirect(base_url().'admin/settings/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->SettingsModel->enable('settings', $id))
			{
				$this->session->set_flashdata('msg', 'Record Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Record');
			}
			redirect(base_url().'admin/settings/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->SettingsModel->disable('settings', $id))
			{
				$this->session->set_flashdata('msg', 'Record Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Record');
			}
			redirect(base_url().'admin/settings/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

}
