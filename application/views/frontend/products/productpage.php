<?php $this->load->view('frontend/layouts/header');?>
<header class="products">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="intro-text">
					<img src="<?= base_url()?>webassets/images/products/product-img.png" class="img-fluid">
				</div>
			</div>
			<div class="col-lg-6 text-right">
				<div class="intro-text">
					<div class="intro-heading text-right"> is simply dummy text of  <br>dummy text </div>
					<p class="banner-txt">Murphy Battery is one of the leading firms in India well known for all types of Automotive ,Stationary Tubular , E-Bike Batteries and More....</p>
				</div>	
			</div>
		</div>
		
	</div>
</header>
<section class="pb-5 pt-3">
	<div class="container">
		<div class="col-lg-12 text-center">
			<h2>Pr<span class="txt-undline">oduc</span>ts</h2>
		</div>
		<div class="row text-center mt-5">
			<?php if ($products): ?>
				<?php foreach ($products as $product): ?>
					<div class="col-md-3 col-sm-6 mb-3">
						<div class="sevice-box">
							<img src="<?= base_url()?>uploads/category/<?= $product['image'] ?>" class="img-fluid mb-3" alt="<?= $product['name'] ?>">
							<div class="mb-3 product-txt"><?= $product['name'] ?></div>
							<a href="<?= base_url()?><?= $product['slug'] ?>" class="btn btn-primary">View Details <i class="fa fa-long-arrow-right"></i></a>
						</div>
					</div>
				<?php endforeach ?>
			<?php endif ?>
		</div>
	</div>
</section>
<section class="bg-light mb-5">
	<div class="container pt-5 pb-5">
		<h2 class="clr-red text-center">Download Product Information</h2>
		<form method="post" action="<?=base_url()?>file-download">
			<div class="row col-lg-10 offset-1 pl-5 pt-3">
				<div class="col-lg-4">
					<div class="form-group">
						<select class="form-control select-area" id="brand" name="brand" onChange="changeBrand(this.value);" required="">
							<option disabled="" selected="true" value="">Brand</option>
							<?php if ($d_brand): ?>
								<?php foreach ($d_brand as $brand): ?>
									<option value="<?= $brand['brand'] ?>"><?= $brand['brand'] ?></option>
								<?php endforeach ?>
							<?php endif ?>
						</select>
						<i class="fa fa-chevron-down"></i>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<select class="form-control select-area" id="product" name="product">
							<option disabled="" selected="">Products</option>
							<!-- <?php if ($d_product): ?>
								<?php foreach ($d_product as $product): ?>
									<option value="<?= $product['product'] ?>"><?= $product['product'] ?></option>
								<?php endforeach ?>
								<?php endif ?> -->
							</select>
							<i class="fa fa-chevron-down"></i>
						</div>
					</div>
					<div class="col-lg-4">
						<button type="submit" class="btn btn-primary btn-download">Download <i class="fa fa-download pl-2"></i></button>
					</div>
				</div>
			</form>
		</div>
	</section>
	<?php $this->load->view('frontend/layouts/enquiry-form');?>
	<?php $this->load->view('frontend/layouts/footer');?>
	<script type="text/javascript">
		function changeBrand(value) {
			if (value.length == 0) document.getElementById("product").innerHTML = "<option></option>";
			else {
				$.ajax({
					url: '<?=base_url()?>productdata',
					type: 'POST',
					data: {value: value},
					error: function() {
						alert('Something is wrong');
					},
					success: function(data) {
						var catOptions="";
						data1 =JSON.parse(data);
						data1.forEach(myFunction);
						function myFunction(item, index) {
							catOptions += "<option>" + item['product'] + "</option>";
						}
						document.getElementById("product").innerHTML = catOptions;
					}
				});
				
			}
		}
	</script>