<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Branch Management
			<small>Add Branch</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/branch/list">Branch</a></li>
			<li class="active">Add Branch</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<?php if($this->session->flashdata('msg')): ?>
					<div class="alert alert-info">
						<strong>Info!</strong> <?php echo $this->session->flashdata('msg') ?>
					</div>
				<?php endif ?>
				<div class="row">
					<form method="post" enctype="multipart/form-data">
						<div class="col-md-12">
							<div class="form-group">
								<label>State</label>
								<select name="state" class="form-control select2" data-placeholder="" style="width: 100%;">
									<?php if ($states): ?>
										<?php foreach ($states as $state): ?>
											<option value="<?= $state ?>" <?php if (set_value('state')==$state): ?>Selected<?php endif ?>><?= $state ?></option>
										<?php endforeach ?>
									<?php endif ?>
								</select>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>City</label>
								<select name="city" class="form-control select2" data-placeholder="" style="width: 100%;">
									<?php if ($cities): ?>
										<?php foreach ($cities as $city): ?>
											<option value="<?= $city ?>" <?php if (set_value('city')==$city): ?>Selected<?php endif ?>><?= $city ?></option>
										<?php endforeach ?>
									<?php endif ?>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Phone Number*</label>
								<input type="text" name="phone" class="form-control" placeholder="Enter Phone Number" value = "<?php echo set_value('phone'); ?>" required >  
								<?php  if(form_error('phone')){echo "<span style='color:red'>".form_error('phone')."</span>";} ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Email *</label>
								<input type="email" name="email" class="form-control" placeholder="Enter  Email Id" value = "<?php echo set_value('email'); ?>" required >  
								<?php  if(form_error('email')){echo "<span style='color:red'>".form_error('email')."</span>";} ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Address *</label>
								<textarea rows="5" class="form-control" name='address' placeholder="Enter Address" required><?php echo set_value('address'); ?></textarea>
								<?php  if(form_error('address')){echo "<span style='color:red'>".form_error('address')."</span>";} ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label> Open Time *</label>
								<input type="text" name="open_time" class="form-control" placeholder="Enter Open Time (Like: 09:30AM TO 8:00PM)" value = "<?php echo set_value('open_time'); ?>" required >  
								<?php  if(form_error('open_time')){echo "<span style='color:red'>".form_error('open_time')."</span>";} ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label> Close Day *</label>
								<input type="text" name="close_day" class="form-control" placeholder="Enter  Close Day (Like: Sunday)" value = "<?php echo set_value('close_day'); ?>" required >  
								<?php  if(form_error('close_day')){echo "<span style='color:red'>".form_error('close_day')."</span>";} ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Status</label>
								<select name="status" class="form-control select2" data-placeholder="" style="width: 100%;">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<?php $this->load->view('layouts/footer');?>