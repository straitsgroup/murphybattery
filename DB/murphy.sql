-- Adminer 4.7.6 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_title` text NOT NULL,
  `category` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `blog_image` varchar(200) DEFAULT NULL,
  `blog_metatitle` text NOT NULL,
  `blog_metadesc` text NOT NULL,
  `blog_metakeyword` text NOT NULL,
  `slug` varchar(100) NOT NULL,
  `blog_schema` text NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `branches`;
CREATE TABLE `branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `address` text,
  `phone` varchar(45) DEFAULT NULL,
  `email` text,
  `open_time` varchar(45) DEFAULT NULL,
  `close_day` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `branches` (`id`, `state`, `city`, `address`, `phone`, `email`, `open_time`, `close_day`, `image`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1,	'Maharashtra',	'Kolhapur',	'Plot No. 1619, Next to Hotel Sonali, Pune Bangalore Road, Shiroli (Pulachi), Kolhapur 416112 \r\n											',	'+91-0230-2462458',	'kolhapur@murphybattery.com',	'09:30AM TO 8:00PM',	'Monday',	NULL,	'1',	'2020-04-27 10:58:50',	'2020-04-27 14:29:47',	'admin',	'admin'),
(2,	'Maharashtra',	'Navi Mumbai',	'Shop # 14 &15, Puneet Corner Co-op. Hsg. Soc.., Vashi 400705\r\n											',	'022-27848586, 32996856',	'navimumbai@murphybattery.com',	'09:30AM TO 8:00PM',	'Sunday',	NULL,	'1',	'2020-04-27 11:21:36',	'2020-04-27 14:28:58',	'admin',	'admin'),
(3,	'Maharashtra',	'Phaltan',	'Gala No. 1 & 2, Garwalia Complex, 3312, Raviwar Peth, Opp. Ranade Petrol Pump, Pune Pandharpur Road, Phaltan 415523\r\n											',	'02166-222544',	'pw@murphybattery.com',	'09:30AM TO 8:00PM',	'Saturday',	NULL,	'1',	'2020-04-27 11:23:01',	'2020-04-27 14:28:35',	'admin',	'admin'),
(4,	'Maharashtra',	'Pune',	'33, Kumar Place, 2408, East Street,  Pune 411001\r\n											',	'020-30528803, 26348212',	'pw@murphybattery.com',	'09:30AM TO 8:00PM',	'Sunday',	NULL,	'1',	'2020-04-27 11:24:30',	'2020-04-27 14:28:02',	'admin',	'admin'),
(5,	'Maharashtra',	'Pune',	'330, Nana Peth, Opp.Chacha Halwai, Pune 411002\r\n											',	' +91-020-26359033, 32931254',	'pw@murphybattery.com',	'09:30AM TO 8:00PM',	'Sunday',	NULL,	'1',	'2020-04-27 11:25:38',	'2020-04-27 14:27:35',	'admin',	'admin'),
(6,	'Maharashtra',	'Pune',	'13/5/939, Behind Hotel Sagar,Kudalwadi,  Chikhali, Pune 410501														',	'020-32947065, 27490001',	'pw@murphybattery.com',	'09:30AM TO 8:00PM',	'Sunday',	NULL,	'1',	'2020-04-27 11:27:43',	'2020-04-27 14:27:44',	'admin',	'admin'),
(7,	'Maharashtra',	'Pune',	'H. No. 3424, Gat No. 888, Kesnand road, Opp. Ayurved CollegeWagholi, Pune 412207',	'020-32947063, 27052766',	'wagholi@murphybattery.com',	'09:30AM TO 8:00PM',	'Sunday',	NULL,	'1',	'2020-04-27 11:28:37',	'2020-04-27 14:26:30',	'admin',	'admin');

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `category` (`id`, `name`, `image`, `description`, `seo_title`, `seo_description`, `seo_keywords`, `cannonical_link`, `slug`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1,	'Automotive',	'automotive-batteries.png',	'<p>Automotive</p>\r\n',	'Automotive',	'Automotive',	'Automotive',	'automotive',	'automotive',	1,	'admin',	NULL,	'2020-04-24 10:52:22',	NULL),
(2,	'Industrial',	'industrial-batteries.png',	'<p>industrial</p>\r\n',	'industrial',	'industrial',	'industrial',	'industrial',	'industrial',	1,	'admin',	NULL,	'2020-04-24 10:53:50',	NULL),
(3,	'E-Bike',	'e-bike.png',	'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n',	'E-Bike',	'E-Bike',	'E-Bike',	'e-bike',	'e-bike',	1,	'admin',	'admin',	'2020-04-24 10:54:58',	'2020-04-24 14:54:02'),
(4,	'UPS',	'ups.png',	'<p>UPS</p>\r\n',	'UPS',	'UPS',	'UPS',	'ups',	'ups',	1,	'admin',	NULL,	'2020-04-24 10:56:23',	NULL),
(5,	'Battery Chargers',	'batter-charger.png',	'<p>Battery Chargers</p>\r\n',	'Battery Chargers',	'Battery Chargers',	'Battery Chargers',	'battery-chargers',	'#',	1,	'admin',	'admin',	'2020-04-24 14:38:52',	'2020-04-27 11:42:56'),
(6,	'Battery Testers',	'battery-tester.png',	'<p>Battery Testers</p>\r\n',	'Battery Testers',	'Battery Testers',	'Battery Testers',	'battery-testers',	'#',	1,	'admin',	'admin',	'2020-04-24 14:40:00',	'2020-04-27 11:42:45'),
(7,	'Our Services',	'our-services.png',	'<p>Our Services</p>\r\n',	'Our Services',	'Our Services',	'Our Services',	'services',	'#',	1,	'admin',	'admin',	'2020-04-24 14:40:44',	'2020-04-27 11:42:35');

DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(2) DEFAULT '1',
  `created_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `clients` (`id`, `name`, `image`, `location`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2,	'Download Management',	'test.png',	'pune',	1,	'admin',	NULL,	'2020-04-10 20:25:09',	NULL);

DROP TABLE IF EXISTS `download`;
CREATE TABLE `download` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(45) DEFAULT NULL,
  `product` text,
  `files` text,
  `status` tinyint(2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `download` (`id`, `brand`, `product`, `files`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1,	'Murphy',	'Automotive',	'1.pdf',	1,	'2020-04-25 00:00:42',	'2020-04-25 00:08:26',	'admin',	'admin'),
(2,	'Murphy',	'Tubular',	'demo.docx',	1,	'2020-04-25 00:10:00',	'2020-04-25 00:17:12',	'admin',	'admin'),
(3,	'Sanso',	'Automotive',	'11.pdf',	1,	'2020-04-25 00:10:43',	NULL,	'admin',	NULL);

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_page_slug_unique` (`page_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `pages` (`id`, `title`, `page_slug`, `description`, `cannonical_link`, `seo_title`, `seo_keyword`, `seo_description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1,	'About Us',	'about-us',	'<p>What is Lorem Ipsum? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n',	'about-us',	'desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',	'desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',	'desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',	1,	'0',	'admin',	'2020-04-09 00:00:00',	'2020-04-24 10:25:20');

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `products` (`id`, `name`, `image`, `description`, `seo_title`, `seo_description`, `seo_keywords`, `cannonical_link`, `slug`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1,	'test',	'test.png',	'<p>test123</p>\r\n',	'test',	'test',	'test',	'test',	'test',	1,	'admin',	'admin',	'2020-04-10 19:27:58',	'2020-04-10 19:28:32');

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` text,
  `email` varchar(50) DEFAULT NULL,
  `phone` text,
  `address` text,
  `url` text,
  `header_files` longtext,
  `facebook` text,
  `linkdin` text,
  `twitter` text,
  `google_plus` text,
  `instagram` text,
  `status` tinyint(2) DEFAULT NULL COMMENT '0=inactive,1=active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `settings` (`id`, `app_name`, `email`, `phone`, `address`, `url`, `header_files`, `facebook`, `linkdin`, `twitter`, `google_plus`, `instagram`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1,	'test',	'test',	'35456',	'test',	'test',	'test',	'test',	'test',	'test',	'test',	'test',	1,	'2020-04-09 00:00:00',	NULL,	'admin',	NULL);

DROP TABLE IF EXISTS `subcategory`;
CREATE TABLE `subcategory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `subcategory` (`id`, `category_id`, `name`, `image`, `description`, `seo_title`, `seo_description`, `seo_keywords`, `cannonical_link`, `slug`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1,	1,	'Murphy Battery',	'murphy.png',	'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n',	'Lorem Ipsum has been the industry\'s standard dummyLorem Ipsum is simply',	'Lorem Ipsum has been the industry\'s standard dummyLorem Ipsum is simply',	'Murphy Battery',	'murphy',	'murphy',	1,	'admin',	NULL,	'2020-04-24 11:00:25',	NULL),
(2,	1,	'Exide Battery',	'exide.png',	'<p>Exide Battery</p>\r\n\r\n<p>&nbsp;</p>\r\n',	'Exide Battery',	'Exide Battery',	'Exide Battery',	'exide',	'exide',	1,	'admin',	NULL,	'2020-04-24 11:01:43',	NULL),
(3,	1,	'Amaron Battery',	'amaron.png',	'<p>Amaron Battery</p>\r\n\r\n<p>&nbsp;</p>\r\n',	'Amaron Battery',	'Amaron Battery',	'Amaron Battery',	'amaron',	'amaron',	1,	'admin',	NULL,	'2020-04-24 11:02:22',	NULL),
(4,	1,	'Sanso Battery',	'sanso.png',	'<p>Sanso Battery</p>\r\n\r\n<p>&nbsp;</p>\r\n',	'Sanso Battery',	'Sanso Battery',	'Sanso Battery',	'sanso',	'sanso',	1,	'admin',	NULL,	'2020-04-24 11:02:56',	NULL),
(5,	2,	'Murphy Battery',	'murphy1.png',	'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n',	'Murphy Battery',	'Murphy Battery',	'Murphy Battery',	'murphy',	'murphy',	1,	'admin',	'admin',	'2020-04-24 14:14:36',	'2020-04-24 14:15:06'),
(6,	2,	'Sanso Battery',	'sanso1.png',	'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n',	'Sanso Battery',	'Sanso Battery',	'Sanso Battery',	'sanso',	'sanso',	1,	'admin',	NULL,	'2020-04-24 14:16:31',	NULL),
(7,	2,	'Exide Battery',	'exide1.png',	'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n',	'Exide Battery',	'Exide Battery',	'Exide Battery',	'exide',	'exide',	1,	'admin',	NULL,	'2020-04-24 14:25:02',	NULL),
(8,	2,	'Quanta Battery',	'amaron1.png',	'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum ha</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum ha</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum ha</p>\r\n',	'Quanta Battery',	'Quanta Battery',	'Quanta Battery',	'quanta',	'quanta',	1,	'admin',	NULL,	'2020-04-24 14:26:17',	NULL),
(9,	3,	'E-Bike',	'e-bike.png',	'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n\r\n<p>&nbsp;</p>\r\n',	'E-Bike',	'E-Bike',	'E-Bike',	'e-bike',	'e-bike',	1,	'admin',	NULL,	'2020-04-24 14:55:18',	NULL),
(10,	3,	'E-Rickshaw',	'e-reckshaw.png',	'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n\r\n<p>&nbsp;</p>\r\n',	'E-Rickshaw',	'E-Rickshaw',	'E-Rickshaw',	'e-rickshaw',	'e-rickshaw',	1,	'admin',	NULL,	'2020-04-24 14:56:05',	NULL),
(11,	4,	'Luminous',	'luminous2.png',	'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n',	'Luminous',	'Luminous',	'Luminous',	'luminous',	'luminous',	1,	'admin',	NULL,	'2020-04-24 16:31:08',	NULL),
(12,	4,	'Microtek',	'microtek1.png',	'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting indust</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting indust</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting indust</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummyLorem Ipsum is simply dummy text of the printing and typesetting indust</p>\r\n\r\n<p>&nbsp;</p>\r\n',	'Microtek',	'Microtek',	'Microtek',	'microtek',	'microtek',	1,	'admin',	NULL,	'2020-04-24 16:35:20',	NULL);

DROP TABLE IF EXISTS `testimonials`;
CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `image` text,
  `title` text,
  `company` text,
  `description` text NOT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` text,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `testimonials` (`id`, `name`, `image`, `title`, `company`, `description`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1,	'Dheeren Padhy',	'testimonial11.png',	'Software Engineer',	'',	'MURPHY BATTERY has definitely earned my trust and confidence. Their batteries power my car and my house and their sales and service are an absolute delight! ',	1,	'2020-04-24 19:26:37',	NULL,	'admin',	NULL),
(2,	'Tushar Ashtekar',	'testimonial2.png',	'Director',	'M.B.Ashtekar Pvt Ltd, Pune',	'I can\'t say enough about the excellent work that \"Murphy Battery\" has done on our site. You took our request and provided us with your product for our new showroom of wagholi. It was absolute pleasure to have services...',	1,	'2020-04-24 19:28:28',	'2020-04-24 19:38:29',	'admin',	'admin');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `last_login` datetime NOT NULL,
  `status` tinyint(2) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `username`, `email`, `password`, `last_login`, `status`, `created_at`) VALUES
(1,	'admin',	'admin@common.com',	'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db',	'2020-04-27 14:25:32',	1,	'2019-10-08'),
(3,	'test',	'test@test.com',	'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db',	'2020-04-10 10:06:37',	1,	'2020-04-09');

-- 2020-04-27 11:19:42
