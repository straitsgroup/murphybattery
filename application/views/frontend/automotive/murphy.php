<?php $this->load->view('frontend/layouts/header');?>
<header class="automotive">
	<div class="container">
		<div class="row text-center">
			<div class="intro-text col-lg-12">
				<h1>Automotive </h1>
			</div>		
		</div>
	</div>
</header>
<section class="bg-light">
	<div class="container py-2">
		<a href="<?= base_url()?>"><i class="fa fa-home clr-red f20 pr-3"></i></a> <i class="fa fa fa-angle-right f20"></i> <a href="<?= base_url()?>automotive" class="pl-3 pr-3"><span class="brd-cum">Automotive</span> </a><i class="fa fa fa-angle-right f20"></i>  <span class="pl-3 brd-cum">Murphy</span>
	</div>
</section>
<section class="pb-5 pt-3">
	<div class="container contact-box">
		<div class="row mt-5">
			<div class="col-md-12 mb-4">
				<h4 class="clr-red mb-3 pt-4 pl-3"><b>Murphy Battery</b></h4>
				<p class="banner-txt px-3 mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
			</div>
		</div>
	</div>
</section>
<section class="mb-5">
	<div class="container">
		<form>
			<div class="row  enq-box py-3">
				<div class="col-md-6 pl-5">
					<h2 class="heading-txt gr-clr">ENQUIRY</h2>
					<div class="form-group pr-5">
						<label class="lbl-txt" for="name">Enter Your Name <span class="clr-red">*</span></label>
						<input type="text" class="form-control" placeholder="Enter Your Name" id="name">
					</div>
					<div class="form-group pr-5">
						<label class="lbl-txt" for="email">Enter Your Email <span class="clr-red">*</span></label>
						<input type="email" class="form-control" placeholder="Enter Your Email" id="email">
					</div>
					<div class="form-group pr-5">
						<label class="lbl-txt" for="phone">Enter Your Phone Number <span class="clr-red">*</span></label>
						<input type="tel" class="form-control" placeholder="Enter Your Phone Number" id="phone">
					</div>
					<div class="form-group pr-5">
						<label class="lbl-txt" for="message">Your Message <span class="clr-red">*</span></label>
						<textarea class="form-control" rows="3" id="message"></textarea>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
				<div class="col-md-6 brd-lft pl-5">
					<br><br>
					<h4 class="clr-red  mt-5"><b>Murphy Battery</b></h4>
					<address class="mt-4">
						32, Kumar Place, <br>
						2408, East Street, <br>
						Pune 411001 <br>
					</address>
					<span>
						<i class="fa fa-phone clr-red pr-2"></i>
						+91-20-30528800, <br>
						<span class="pl-4">+91-20-26348212</span>
					</span><br><br>
					<span>
						<i class="fa fa-envelope clr-red pr-2"></i>
						pw@murphybattery.com
					</span>
				</div>
			</div>
		</form>
	</div>
</section>
<?php $this->load->view('frontend/layouts/footer');?>