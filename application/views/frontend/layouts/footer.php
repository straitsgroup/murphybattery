<!-- Footer -->
<div id="news-msg">
	<?php if($this->session->flashdata('news_email')): ?>
		<div class="alert alert-info">
			<strong>Info!</strong> <?php echo $this->session->flashdata('news_email') ?>
		</div>
	<?php endif ?>
	<?php if($this->session->flashdata('news_email_err')): ?>
		<div class="alert alert-danger">
			<strong>Info!</strong> <?php echo $this->session->flashdata('news_email_err') ?>
		</div>
	<?php endif ?>
</div>
<footer class="footer">
	<div class="container">
		<div class="row pt-5">
			<div class="col-md-3">
				<h5 class="clr-red">About us</h5>
				<ul class="list-unstyled">
					<li><a href="<?= base_url()?>about-us#vision">Vision</a></li>
					<li><a href="<?= base_url()?>about-us#mission">Mission</a></li>
					<li><a href="<?= base_url()?>about-us#values">Values</a></li>
					<li><a href="<?= base_url()?>about-us#testimonial">Testimonials</a></li>
				</ul>
				<h5 class="clr-red">Contact us</h5>
				<ul class="list-unstyled">
					<li><a href="<?= base_url()?>contact-us">Contact us</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<h5 class="clr-red">Automotive</h5>
				<ul class="list-unstyled">
					<li><a href="<?= base_url()?>automotive/murphy">Murphy</a></li>
					<li><a href="<?= base_url()?>automotive/exide">Exide</a></li>
					<li><a href="<?= base_url()?>automotive/amaron">Amaron</a></li>
					<li><a href="<?= base_url()?>automotive/sanso">Sanso</a></li>
				</ul>
				<h5 class="clr-red">UPS</h5>
				<ul class="list-unstyled">
					<li><a href="<?= base_url()?>ups/luminous">Luminous</a></li>
					<li><a href="<?= base_url()?>ups/microtek">Microtek</a></li>
				</ul>
				
			</div>
			<div class="col-md-3">
				<h5 class="clr-red">Industrial</h5>
				<ul class="list-unstyled">
					<li><a href="<?= base_url()?>industrial/murphy">Murphy</a></li>
					<li><a href="<?= base_url()?>industrial/sanso">Sanso</a></li>
					<li><a href="<?= base_url()?>industrial/exide">Exide</a></li>
					<li><a href="<?= base_url()?>industrial/quanta">Quanta</a></li>
				</ul>
				<h5 class="clr-red">E-Bike / E-Rickshaw</h5>
				<ul class="list-unstyled">
					<li><a href="#">Trontek</a></li>
				</ul>
			</div>

			<div class="col-md-3 mb-3">
				<img src="<?= base_url()?>webassets/images/logo.png" class="img-fluid logo">
				<h5 class="clr-red mt-4">Follow us</h5>
				<a href="#"><span class="mr-2 social-icon"><i class="fa fa-facebook px-1"></i></span></a>
				<a href="#"><span class="mr-2 social-icon"><i class="fa fa-linkedin"></i></span></a>
				<a href="#"><span class="mr-2 social-icon"><i class="fa fa-whatsapp"></i></span></a>
				<a href="#"><span class="mr-2 social-icon"><i class="fa fa-instagram"></i></span></a>
				<p class="clr-red mt-4">Subscribe to our Newsletter</p>
				<form method="post" action="<?= base_url('newsletter')?>">
					<div class="input-group mb-3">
						<input type="email" name="email" class="form-control" placeholder="Email Id" required>
						<div class="input-group-append">
							<button class="btn btn-primary" type="submit">Submit</button>
						</div>
					</div>
				</form>
				<!-- <p class="clr-red mt-3 mb-0">Looking to buy a Battery?</p>
					<a href="#">Quick Buy</a> -->
				</div>
			</div>
		</div>
		<hr class="hr">
		<div class="container">
			<div class="row text-center">
				<div class="col-md-12">
					<span class="copyright"> &copy; All Rights Reserved By Murphy Battery | Powered By <a href="https://www.enayble.com/" target="_blank" class="enayble">Enayble.com</a></span>
				</div>        
			</div>
		</div>
	</footer>
	<script src="<?= base_url()?>webassets/vendor/jquery/jquery.min.js"></script>
	<script src="<?= base_url()?>webassets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?= base_url()?>webassets/js/custom.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			var url = window.location;
			$('li.navbar-nav a[href="'+ url +'"]').parent().addClass('active');
			$('ul.navbar-nav a').filter(function() {
				return this.href == url;
			}).addClass('active');

		});
	</script> 
</body>
</html>