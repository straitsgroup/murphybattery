<?php $this->load->view('frontend/layouts/header');?>
<header class="automotive">
	<div class="container">
		<div class="row text-center">
			<div class="intro-text col-lg-12">
				<h1><?= $ebike['name']  ?></h1>
			</div>		
		</div>
	</div>
</header>
<section class="bg-light">
	<div class="container py-2">
		<a href="<?= base_url()?>"><i class="fa fa-home clr-red f20 pr-3"></i></a> <i class="fa fa fa-angle-right f20"></i> <span class="pl-3 brd-cum"><?= $ebike['name']  ?></span>
	</div>
</section>
<section class="my-5">
	<div class="container indus-box py-3">
		<div class="row">
			<div class="col-lg-12 px-4 pt-4">
				<h4 class="clr-red"><b><?= $ebike['name']  ?></b></h4>
				<div class="banner-txt mb-4 text-justify"><?= $ebike['description']; ?></div>
			</div>

			<?php if ($sub_autmtve): ?>
				<?php foreach ($sub_autmtve as $Record): ?>
					<div class="col-lg-6  text-center">
						<div class="m-3 bg-light p-5">
							<img src="<?= base_url()?>uploads/subcategory/<?= $Record['image'] ?>" class="img-fluid mb-3" alt="<?= $Record['name'] ?>">
							<h4><?= $Record['name'] ?></h4>
							<div class="banner-txt mb-4 text-justify"><?= substr(strip_tags($Record['description']),0,120) ?></div>
							<a href="<?= base_url()?>e-bike/<?= $Record['slug'] ?>" class="btn btn-primary">Read More <i class="fa fa-long-arrow-right"></i></a>
						</div>
					</div>
				<?php endforeach ?>
			<?php endif ?>
		</div>
	</div>
</section>
<section class="bg-light mb-5">
	<div class="container pt-5 pb-5 text-center">
		<div class="col-lg-10 offset-1">
			<h2 class="gr-clr">Pr<span class="txt-undline">oduc</span>ts</h2>
			<div class="row pt-3">
				<div class="col-lg-4">
					<img src="<?= base_url()?>webassets/images/e-bike/product1.png" class="img-fluid">
				</div>
				<div class="col-lg-4">
					<img src="<?= base_url()?>webassets/images/e-bike/product2.png" class="img-fluid">
				</div>
				<div class="col-lg-4">
					<img src="<?= base_url()?>webassets/images/e-bike/product3.png" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('frontend/layouts/enquiry-form');?>
<?php $this->load->view('frontend/layouts/footer');?>