<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DownloadController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('DownloadModel');
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->DownloadModel->getAll('download'); 
			$data['pagetitle'] = 'Download List';
			$this->load->view('download/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/download/';
				$config['allowed_types'] = 'pdf|docx';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('files'))
				{
					$files = $this->upload->data()['file_name'];
				}
				else
				{
					$files = NULL;
				}

				$data = array(
					'files' =>$files , 
					'brand' =>$this->input->post('brand') , 
					'product' =>$this->input->post('product') , 
					'status' =>$this->input->post('status') , 
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->DownloadModel->add($data, 'download'))
				{
					$this->session->set_flashdata('msg', 'Download Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Download');
				}

				redirect(base_url().'admin/download/add');

			}else{
				$data['pagetitle'] = 'Add Download';
				$this->load->view('download/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/download/';
				$config['allowed_types'] = 'pdf|doc|docx';
				$this->load->library('upload', $config);
				$config['max_size'] = 4096;
				$this->upload->initialize($config);

				if ($this->upload->do_upload('files'))
				{
					$files = $this->upload->data()['file_name'];
					$data = array(
						'files' =>$files , 
						'brand' =>$this->input->post('brand') , 
						'product' =>$this->input->post('product') , 
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 

					);
				}
				else
				{
					$data = array(
						'brand' =>$this->input->post('brand') , 
						'product' =>$this->input->post('product') , 
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 

					);
				}


				
				if ($this->DownloadModel->edit($data, 'download', $id))
				{
					$this->session->set_flashdata('msg', 'Download Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Download');
				}
				redirect(base_url().'admin/download/list');

			}else{
				$data['pagetitle'] = 'Edit Download';
				$data['Record'] = $this->DownloadModel->getById('download', $id);
				$this->load->view('download/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->DownloadModel->delete('download', $id))
			{
				$this->session->set_flashdata('msg', 'Download Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Download');
			}
			redirect(base_url().'admin/download/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->DownloadModel->enable('download', $id))
			{
				$this->session->set_flashdata('msg', 'Download Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Download');
			}
			redirect(base_url().'admin/download/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->DownloadModel->disable('download', $id))
			{
				$this->session->set_flashdata('msg', 'Download Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Download');
			}
			redirect(base_url().'admin/download/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

}
