<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Download Management
			<small>Edit Download</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/download/list">Download</a></li>
			<li class="active">Edit Download</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<form method="post" enctype="multipart/form-data">
						<div class="col-md-12">
							<div class="form-group">
								<label>File</label>
								<input type='file' class="form-control" name="files" value="<?=$Record['files']?>" >
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Brand *</label>
								<input type="text" name="brand" class="form-control" placeholder="Enter Brand" value="<?=$Record['brand']?>" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Product *</label>
								<input type="text" name="product" class="form-control" placeholder="Enter Product" value="<?=$Record['product']?>" required>
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>

				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layouts/footer');?>