<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BranchController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('BranchModel');
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->BranchModel->getAll('branches'); 
			$data['pagetitle'] = 'Branch List';
			$this->load->view('branch/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$this->form_validation->set_rules('state', 'State', 'required');
				$this->form_validation->set_rules('city', 'city', 'required');
				$this->form_validation->set_rules('address', 'address', 'trim');
				$this->form_validation->set_rules('phone', 'Phone', 'numeric|required');
				$this->form_validation->set_rules('email', 'Email', 'valid_emails|required');
				$this->form_validation->set_rules('open_time', 'open_time', 'required');
				$this->form_validation->set_rules('close_day', 'close_day', 'required');
				$this->form_validation->set_rules('msg', 'Message', 'trim');
				if ($this->form_validation->run() == FALSE) {
					$states=['Maharashtra'];
					$cities=['Pune','Navi Mumbai','Kolhapur','Phaltan'];

					$data['states']=$states;
					$data['cities']=$cities;
					$data['pagetitle'] = 'Add Branch';
					$this->load->view('branch/add', $data);	
				}else{
					$data = array(
						'state' =>$this->input->post('state') , 
						'city' =>$this->input->post('city') , 
						'address' =>$this->input->post('address') , 
						'phone' =>$this->input->post('phone') , 
						'email' =>$this->input->post('email') , 
						'open_time' =>$this->input->post('open_time') , 
						'close_day' =>$this->input->post('close_day') , 
						'status' =>$this->input->post('status') , 
						'created_at'=>date("Y-m-d H:i:s"),
						'created_by' => $this->session->userdata('username'),
					);
					if ($this->BranchModel->add($data, 'branches'))	{
						$this->session->set_flashdata('msg', 'Branch Added Successfully');
					}
					else{
						$this->session->set_flashdata('msg', 'Error Adding Branch');
					}
					redirect(base_url().'admin/branch/add');
				}
			}else{
				$states=['Maharashtra'];
				$cities=['Pune','Navi Mumbai','Kolhapur','Phaltan'];

				$data['states']=$states;
				$data['cities']=$cities;
				$data['pagetitle'] = 'Add Branch';
				$this->load->view('branch/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$this->form_validation->set_rules('state', 'State', 'required|alpha_numeric_spaces');
				$this->form_validation->set_rules('city', 'city', 'required|alpha_numeric_spaces');
				$this->form_validation->set_rules('address', 'address', 'trim');
				$this->form_validation->set_rules('phone', 'Phone', 'numeric|required');
				$this->form_validation->set_rules('email', 'Email', 'valid_emails|required');
				$this->form_validation->set_rules('open_time', 'open_time', 'required');
				$this->form_validation->set_rules('close_day', 'close_day', 'required');
				$this->form_validation->set_rules('msg', 'Message', 'trim');
				if ($this->form_validation->run() == FALSE) {
					$data['pagetitle'] = 'Edit Branch';
					$states=['Maharashtra'];
					$cities=['Pune','Navi Mumbai','Kolhapur','Phaltan'];
					$data['states']=$states;
					$data['cities']=$cities;
					$data['Record'] = $this->BranchModel->getById('branches', $id);
					$this->load->view('branch/edit', $data);
				}else{
					$data = array(
						'state' =>$this->input->post('state') , 
						'city' =>$this->input->post('city') , 
						'address' =>$this->input->post('address') , 
						'phone' =>$this->input->post('phone') , 
						'email' =>$this->input->post('email') , 
						'open_time' =>$this->input->post('open_time') , 
						'close_day' =>$this->input->post('close_day') , 
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 
					);
					if ($this->BranchModel->edit($data, 'branches', $id))
					{
						$this->session->set_flashdata('msg', 'Branch Edited Successfully');
					}
					else
					{
						$this->session->set_flashdata('msg', 'Error Editing Branch');
					}
					redirect(base_url().'admin/branch/list');
				}
			}else{
				$data['pagetitle'] = 'Edit Branch';
				$states=['Maharashtra'];
				$cities=['Pune','Navi Mumbai','Kolhapur','Phaltan'];
				$data['states']=$states;
				$data['cities']=$cities;
				$data['Record'] = $this->BranchModel->getById('branches', $id);
				$this->load->view('branch/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->BranchModel->delete('branches', $id))
			{
				$this->session->set_flashdata('msg', 'Branch Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Branch');
			}
			redirect(base_url().'admin/branch/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->BranchModel->enable('branches', $id))
			{
				$this->session->set_flashdata('msg', 'Branch Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Branch');
			}
			redirect(base_url().'admin/branch/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->BranchModel->disable('branches', $id))
			{
				$this->session->set_flashdata('msg', 'Branch Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Branch');
			}
			redirect(base_url().'admin/branch/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

}
