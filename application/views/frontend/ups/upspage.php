<?php $this->load->view('frontend/layouts/header');?>
<header class="automotive">
	<div class="container">
		<div class="row text-center">
			<div class="intro-text col-lg-12">
				<h1><?= $ups['name']  ?></h1>
			</div>		
		</div>
	</div>
</header>
<section class="bg-light">
	<div class="container py-2">
		<a href="<?= base_url()?>"><i class="fa fa-home clr-red f20 pr-3"></i></a> <i class="fa fa fa-angle-right f20"></i> <span class="pl-3 brd-cum"><?= $ups['name']  ?></span>
	</div>
</section>
<?php if ($sub_autmtve): ?>
	<?php foreach ($sub_autmtve as $Record): ?>
		<section class="my-5">
			<div class="container indus-box py-3">
				<div class="row">
					<div class="col-lg-12 pl-5 pt-2">
						<h2 class="gr-clr"><b><?= $Record['name'] ?></b></h2>
					</div>
					<div class="col-lg-5 text-center">
						<div class="pt-3">
							<img src="<?= base_url()?>uploads/subcategory/<?= $Record['image'] ?>" class="img-fluid mb-3 ups-img" alt="<?= $Record['name'] ?>">
						</div>
					</div>
					<div class="col-lg-7  text-center">
						<div class="pr-5">
							<div class="banner-txt mb-4 text-justify"><?= substr(strip_tags($Record['description']),0,200) ?></div>
							<a href="<?= base_url()?>ups/<?= $Record['slug'] ?>" class="btn btn-primary float-right">Read More <i class="fa fa-long-arrow-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endforeach ?>
<?php endif ?>
<?php $this->load->view('frontend/layouts/enquiry-form');?>
<?php $this->load->view('frontend/layouts/footer');?>