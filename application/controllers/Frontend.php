<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('FrontendModel');
	}
	
	public function index()	{
		$data['seo_title'] = "Home";
		$data['seo_description'] = "Home";
		$data['seo_keywords'] = "Home";
		$data['cannonical_link'] = base_url();
		$this->load->view('frontend/home', $data);
	}

	public function aboutUs()	{
		$data['seo_title'] = "About us";
		$data['seo_description'] = "About us";
		$data['seo_keywords'] = "About us";
		$data['cannonical_link'] = base_url('about-us');
		$data['testimonials'] = $this->FrontendModel->getAll('testimonials'); 
		$this->load->view('frontend/aboutus', $data);
	}
	public function contactUs()	{
		if($this->input->post()){
			$this->form_validation->set_rules('name', 'Name', 'required|alpha_numeric_spaces');
			$this->form_validation->set_rules('email', 'Email', 'valid_emails|required');
			$this->form_validation->set_rules('phone', 'Phone', 'numeric|required');
			$this->form_validation->set_rules('msg', 'Message', 'trim');
			if ($this->form_validation->run() == FALSE) {
				$states=['Maharashtra'];
				$cities=['Pune','Navi Mumbai','Kolhapur','Phaltan'];

				$data['states']=$states;
				$data['cities']=$cities;
				$data['seo_title'] = "Contact Us";
				$data['seo_description'] = "Contact Us";
				$data['seo_keywords'] = "Contact Us";
				$data['cannonical_link'] = base_url('contact-us');
				$this->load->view('frontend/contactus', $data);
			}else{

				$this->load->config('email');
				$this->load->library('email');

				$to = $this->config->item('smtp_user');
				$from = $this->input->post('email');
				$this->email->from($from);
				$this->email->to($to);
				$this->email->subject("Contact Us Form: Enquiry");

				$maildata = "
				Hello There E- Mail For You,<br>
				<table>
				<tr>
				<td><b>Name</b> : </td>
				<td>".$this->input->post('name')."</td>
				</tr>
				<tr>
				<td><b>Email</b> : </td>
				<td>".$this->input->post('email')."</td>
				</tr>
				<tr>
				<td><b>Phone</b> : </td>
				<td>".$this->input->post('phone')."</td>
				</tr>
				<tr>
				<td><b>Message</b> : </td>
				<td>".$this->input->post('msg')."</td>
				</tr>
				</table>";


				$this->email->set_newline("\r\n");
				$this->email->message($maildata);

				if($this->email->send()) 
					$this->session->set_flashdata("email_sent","Email sent successfully."); 
				else 
					$this->session->set_flashdata("email_sent_err","Error in sending Email."); 
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
		else{
			$states=['Maharashtra'];
			$cities=['Pune','Navi Mumbai','Kolhapur','Phaltan'];

			$data['states']=$states;
			$data['cities']=$cities;
			$data['seo_title'] = "Contact Us";
			$data['seo_description'] = "Contact Us";
			$data['seo_keywords'] = "Contact Us";
			$data['cannonical_link'] = base_url('contact-us');
			$this->load->view('frontend/contactus', $data);
		}
	}

	/*Product Page*/
	public function Products()	{
		$data['seo_title'] = "Products";
		$data['seo_description'] = "Products";
		$data['seo_keywords'] = "Products";
		$data['cannonical_link'] = base_url('products');

		$data['products'] = $this->FrontendModel->getAll('category','asc'); 
		$data['d_product'] = $this->FrontendModel->getGroup('download','product','asc'); 
		$data['d_brand'] = $this->FrontendModel->getGroup('download','brand','asc'); 
		$this->load->view('frontend/products/productpage', $data);
	}
	/*Product Sub Pages*/
	public function productSubpages($slug){
		$product = $this->FrontendModel->getBySlug('category',$slug); 
		$data['product']=$product;
		$data['seo_title'] = $product['seo_title'];
		$data['seo_description'] = $product['seo_description'];
		$data['seo_keywords'] = $product['seo_keywords'];
		$data['cannonical_link'] = base_url($product['cannonical_link']);
		$this->load->view('frontend/products/subpages', $data);
	}

	/*Automotive Pages*/
	public function Automotive(){
		$automotive = $this->FrontendModel->getBySlug('category','automotive'); 
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$automotive['id']); 

		$data['automotive']=$automotive;
		$data['pagetitle'] = "Automotive";

		$data['seo_title'] = $automotive['seo_title'];
		$data['seo_description'] = $automotive['seo_description'];
		$data['seo_keywords'] = $automotive['seo_keywords'];
		$data['cannonical_link'] = base_url($automotive['cannonical_link']);

		$this->load->view('frontend/automotive/automotivepage', $data);
	}

	/*Automotive Sub Pages*/
	public function automotiveSubpages($slug){
		$automotive = $this->FrontendModel->getBySlug('category','automotive'); 
		$sub_page = $this->FrontendModel->getSubPageBySlug('subcategory','category_id',$automotive['id'],$slug); 
		$data['sub_page']=$sub_page;
		$data['automotive']=$automotive;
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$automotive['id']); 
		$data['pagetitle'] = $sub_page['name'];

		$data['seo_title'] = $sub_page['seo_title'];
		$data['seo_description'] = $sub_page['seo_description'];
		$data['seo_keywords'] = $sub_page['seo_keywords'];
		$data['cannonical_link'] = base_url($sub_page['cannonical_link']);
		$this->load->view('frontend/automotive/subpages', $data);
	}

	/*Industrial Pages*/
	public function Industrial(){
		$industrial = $this->FrontendModel->getBySlug('category','industrial'); 
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$industrial['id']); 
		$data['industrial']=$industrial;

		$data['seo_title'] = $industrial['seo_title'];
		$data['seo_description'] = $industrial['seo_description'];
		$data['seo_keywords'] = $industrial['seo_keywords'];
		$data['cannonical_link'] = base_url($industrial['cannonical_link']);
		$this->load->view('frontend/industrial/industrialpage', $data);
	}

	/*Industrial Sub Pages*/
	public function industrialSubpages($slug){
		$industrial = $this->FrontendModel->getBySlug('category','industrial'); 
		$sub_page = $this->FrontendModel->getSubPageBySlug('subcategory','category_id',$industrial['id'],$slug); 
		$data['sub_page']=$sub_page;
		$data['industrial']=$industrial;
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$industrial['id']); 

		$data['seo_title'] = $sub_page['seo_title'];
		$data['seo_description'] = $sub_page['seo_description'];
		$data['seo_keywords'] = $sub_page['seo_keywords'];
		$data['cannonical_link'] = base_url($sub_page['cannonical_link']);
		$this->load->view('frontend/industrial/subpages', $data);
	}

	/*Lithium Pages*/
	public function Lithium(){
		$lithium = $this->FrontendModel->getBySlug('category','lithium'); 
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$lithium['id']); 

		$data['lithium']=$lithium;
		$data['pagetitle'] = "lithium";

		$data['seo_title'] = $lithium['seo_title'];
		$data['seo_description'] = $lithium['seo_description'];
		$data['seo_keywords'] = $lithium['seo_keywords'];
		$data['cannonical_link'] = base_url($lithium['cannonical_link']);

		$this->load->view('frontend/lithium/lithiumpage', $data);
	}

	/*Lithium Sub Pages*/
	public function LithiumSubpages($slug){
		$lithium = $this->FrontendModel->getBySlug('category','lithium'); 
		$sub_page = $this->FrontendModel->getSubPageBySlug('subcategory','category_id',$lithium['id'],$slug); 
		$data['sub_page']=$sub_page;
		$data['lithium']=$lithium;
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$lithium['id']); 
		$data['pagetitle'] = $sub_page['name'];

		$data['seo_title'] = $sub_page['seo_title'];
		$data['seo_description'] = $sub_page['seo_description'];
		$data['seo_keywords'] = $sub_page['seo_keywords'];
		$data['cannonical_link'] = base_url($sub_page['cannonical_link']);
		$this->load->view('frontend/lithium/subpages', $data);
	}


	/*E-Bike Sub Pages*/

	public function eBike(){
		$ebike = $this->FrontendModel->getBySlug('category','e-bike'); 
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$ebike['id']); 
		$data['ebike']=$ebike;

		$data['seo_title'] = $ebike['seo_title'];
		$data['seo_description'] = $ebike['seo_description'];
		$data['seo_keywords'] = $ebike['seo_keywords'];
		$data['cannonical_link'] = base_url($ebike['cannonical_link']);

		$this->load->view('frontend/ebike/ebikepage', $data);
	}
	/*E-bike Sub Pages*/

	public function eBikeSubpages($slug){
		$ebike = $this->FrontendModel->getBySlug('category','e-bike'); 
		$sub_page = $this->FrontendModel->getSubPageBySlug('subcategory','category_id',$ebike['id'],$slug); 
		$data['sub_page']=$sub_page;
		$data['ebike']=$ebike;
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$ebike['id']); 

		$data['seo_title'] = $sub_page['seo_title'];
		$data['seo_description'] = $sub_page['seo_description'];
		$data['seo_keywords'] = $sub_page['seo_keywords'];
		$data['cannonical_link'] = base_url($sub_page['cannonical_link']);
		$this->load->view('frontend/ebike/subpages', $data);
	}
	/*UPS Page*/
	public function Ups(){
		$data['pagetitle'] = "UPS";
		$ups = $this->FrontendModel->getBySlug('category','ups'); 
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$ups['id']); 
		$data['ups']=$ups;

		$data['seo_title'] = $ups['seo_title'];
		$data['seo_description'] = $ups['seo_description'];
		$data['seo_keywords'] = $ups['seo_keywords'];
		$data['cannonical_link'] = base_url($ups['cannonical_link']);
		$this->load->view('frontend/ups/upspage', $data);
	}

	/*UPS Sub Pages*/

	public function upsSubpages($slug){
		$ups = $this->FrontendModel->getBySlug('category','ups'); 
		$sub_page = $this->FrontendModel->getSubPageBySlug('subcategory','category_id',$ups['id'],$slug); 
		$data['sub_page']=$sub_page;
		$data['ups']=$ups;
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$ups['id']); 

		$data['seo_title'] = $sub_page['seo_title'];
		$data['seo_description'] = $sub_page['seo_description'];
		$data['seo_keywords'] = $sub_page['seo_keywords'];
		$data['cannonical_link'] = base_url($sub_page['cannonical_link']);
		$this->load->view('frontend/ups/subpages', $data);
	}

	public function downloadFiles(){
		$data['pagetitle'] = "Download Files";

		$data['seo_title'] = "Download Files";
		$data['seo_description'] = "Download Files";
		$data['seo_keywords'] = "Download Files";
		$data['cannonical_link'] = base_url("download");

		$data['downloads'] = $this->FrontendModel->getAll('download','asc'); 
		$this->load->view('frontend/download', $data);
	}


	public function EnqForm(){
		if($this->input->post()){
			$this->form_validation->set_rules('name', 'Name', 'required|alpha_numeric_spaces');
			$this->form_validation->set_rules('email', 'Email', 'valid_emails|required');
			$this->form_validation->set_rules('phone', 'Phone', 'numeric|required');
			$this->form_validation->set_rules('msg', 'Message', 'trim');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER'].'#error');
			}else{
				$this->load->config('email');
				$this->load->library('email');

				$to = $this->config->item('smtp_user');
				$from = $this->input->post('email');
				$this->email->from($from);
				$this->email->to($to);
				$this->email->subject("Enquiry From: Enquiry");

				$maildata = "
				Hello There E- Mail For You,<br>
				<table>
				<tr>
				<td><b>Name</b> : </td>
				<td>".$this->input->post('name')."</td>
				</tr>
				<tr>
				<td><b>Email</b> : </td>
				<td>".$this->input->post('email')."</td>
				</tr>
				<tr>
				<td><b>Phone</b> : </td>
				<td>".$this->input->post('phone')."</td>
				</tr>
				<tr>
				<td><b>Message</b> : </td>
				<td>".$this->input->post('msg')."</td>
				</tr>
				</table>";


				$this->email->set_newline("\r\n");
				$this->email->message($maildata);

				if($this->email->send()) 
					$this->session->set_flashdata("email_sent","Email sent successfully."); 
				else 
					$this->session->set_flashdata("email_sent_err","Error in sending Email."); 
			}
			redirect($_SERVER['HTTP_REFERER']."#msgdata");

		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	public function newsLetter(){
		if($this->input->post()){
			$this->load->config('email');
			$this->load->library('email');

			$to = $this->config->item('smtp_user');
			$from = $this->input->post('email');
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject("Newsletter Email");

			$maildata = "
			Hello There E- Mail For You,<br>
			<table>
			<tr>
			<td><b>Email</b> : </td>
			<td>".$this->input->post('email')."</td>
			</tr>
			</table>";

			$this->email->set_newline("\r\n");
			$this->email->message($maildata);

			if($this->email->send()) 
				$this->session->set_flashdata("news_email","Email sent successfully."); 
			else 
				$this->session->set_flashdata("news_email_err","Error in sending Email."); 

			redirect($_SERVER['HTTP_REFERER']."#news-msg");

		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	public function filter(){
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$data = $this->FrontendModel->getBranch('branches',$state,$city); 
		echo json_encode($data);
	}
	public function fileDownload()	{
		if ($this->input->post()) {
			$this->load->helper('download');
			$brand =$this->input->post('brand');
			$product = $this->input->post('product');
			if (empty($brand) || empty($product)) {
				redirect($_SERVER['HTTP_REFERER']);
			}
			$data = $this->FrontendModel->getFile('download',$brand,$product); 
			$fileName= $data['files'];
			if ($fileName) {
				$file = FCPATH .'/uploads/download/'. $fileName;
				$data = file_get_contents ( $file );
				force_download ( $fileName, $data );
				redirect($_SERVER['HTTP_REFERER']);
			}else{
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	public function productData(){
		$brand = $this->input->post('value');
		$data = $this->FrontendModel->getProduct('download',$brand); 
		echo json_encode($data);
	}
}